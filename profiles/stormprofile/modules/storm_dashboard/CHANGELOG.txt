/* $Id*/


storm_dashboard 6.x-1.0, 2009-12-07
-----------------------------------
- Intial Public Release
- FEATURE: Using filter "No filter" now means that all objects are selected and not filtered

storm_dashboard 6.x-1.x, 2009-11-30 (development release)
---------------------------------------------------------
- BUG: on project level only display tasks with no parent task
- BUG: properly support node revisions
- FEATURE: highlight new nodes and nodes with new comments in lists within the dashboard
- FEATURE: improved rendering of nodes in the dashboard to support comments and links
- FETAURE: support Storm's new css file
- FEATURE: order tasks and tickets by priority

storm_dashboard 6.x-1.x, 2009-11-15 (development release)
---------------------------------------------------------
- FEATURE #630916 by amv101: Implemented an icon in the bottom left pane to allow toggling of the dashboard in the lightbox
- BUG #630916 by amv101: breadcrumb only gets updated when dashboard is in mainview, not in the lightbox

storm_dashboard 6.x-1.x, 2009-11-12 (development release)
---------------------------------------------------------
- FEATURE #630916 by amv101: Show the dashboard in a lightbox so that main column width is no longer a limitation

storm_dashboard 6.x-1.x, 2009-11-03 (development release)
---------------------------------------------------------
- BUG: Corrected table shortnames in queries to make sure hook_access_sql works properly for non user1 users

storm_dashboard 6.x-1.x, 2009-11-03 (development release)
---------------------------------------------------------
- FEATURE: Included filters so that only organizations, projects, tasks and tickets of defined stati get displayed. These filters are stored on a per-user-basis.
- FEATURE: Included a checkbox next to the organization drop down to switch on/off the auto-collapse of the organization panel as soon as a project gets selected. This setting is stored on a per-session-basis.
- CHANGE: Theme of timetracking box (bottom left corner by default) got changed
- IMPORTANT: Please also update the module storm_quicktt!!!

storm_dashboard 6.x-1.x, 2009-09-17 (development release)
---------------------------------------------------------
- BUG: Force QuickTT to load JS, CSS and settings but make sure that only happens once

storm_dashboard 6.x-1.x, 2009-09-16 (development release)
---------------------------------------------------------
- BUG: Timer wasn't started when timetracking got initiated by a storm_dashboard component
- BUG 578726 by gnu4ever and GiorgosK: Provide the URL for JQuery AJAX dynamically

storm_dashboard 6.x-1.x, 2009-09-15 (development release)
---------------------------------------------------------
- Improved integration with storm_quicktt

storm_dashboard 6.x-1.x, 2009-09-11 (development release)
---------------------------------------------------------
- FEATURE: Timetracking icons and info box (bottom left) is only displayed if user has permission to add timetracking
- BUG 568988 by Mark_Watson27: TimeTracking wasn't displayed on the ticket level
- BUG 568988 by Mark_Watson27: Title for TimeTracking and Notes implemented to be consistent with other blocks
- FEATURE 573952 by deggertsen: Make the blocks themeable so that you can omit certain parts or rearrange the elements
- FEATURE: The error is now giving more detailed information if there is any unexpected feedback from the server
- FEATURE: The module now checks for individual Storm modules being installed and if not, it just leaves the relevant block empty instead of creating an error message

storm_dashboard 6.x-1.x, 2009-09-10 (development release)
---------------------------------------------------------
- BUG 568988 by deggertsen: Improved CSS to make default display also work in Garland theme
- FEATURE 573946 by deggertsen: Implement a dynamic breadcrumb using the theme breadcrumb to navigate back in hierarchy

storm_dashboard 6.x-1.x, 2009-09-07 (development release)
---------------------------------------------------------
- BUG: Show org components when selecting another org while org details where hidden
- BUG 568606: possibly fixed, was a missing leading slash in the URL for AJAX
- FEATURE: titles for projects, tasks and tickets are now links to their respective nodes
- FEATURE: the URL http://www.example.com/stormdashboard now allows another parameter for the nid to be loaded. This helps to create shortcuts into deep levels of the dashboard
- FEATURE: organization panel closes when a project gets loaded (to save space) and a toggle button got inserted
- CHANGE: reflected theming changes in the latest dev version of Storm

storm_dashboard 6.x-1.x, 2009-09-05 (development release)
---------------------------------------------------------
- BUG: refresh display of active timetracking when returning to the dashboard or loading child objects. Please update to the latest storm_quicktt as well to make this work

storm_dashboard 6.x-1.x, 2009-09-05 (development release)
---------------------------------------------------------
- BUG 568606 and 568944: Included JSON2.JS to make the module work also on older browsers as well as browsers with no native json support like Chrome and Safari.

storm_dashboard 6.x-1.x, 2009-09-02 (development release)
---------------------------------------------------------
- Initial development release
