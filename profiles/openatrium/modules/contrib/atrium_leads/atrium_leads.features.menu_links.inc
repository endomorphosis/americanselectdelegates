<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function atrium_leads_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: features:leads
  $menu_links['features:leads'] = array(
    'menu_name' => 'features',
    'link_path' => 'leads',
    'router_path' => 'leads',
    'link_title' => 'Leads',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '10',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Leads');


  return $menu_links;
}
