--------------------------
Open Atrium Business Leads
--------------------------
atrium_leads-6.x-1.0-beta1
--------------------------
Developed By:

  ThinkDrop Consulting LLC
  thinkdrop.net
  @thinkdropLLC

  Jonathan Pugh
  jon@thinkdrop.net
  @careernerd
--------------------------

This feature creates a basic content type called "Business Lead" that can be used
for anything that needs a link, contact info, and a status selector.

You can edit the possible statuses by editing that field in the Content Types 
administration pages.

--------------------------

Requirements:
  OpenAtrium beta5 [ http://openatrium.com ]

Features:
  # Content Type: Business Lead (lead)
    - Fields: Title, Body, Status, Link, Contacts
    - Settings included for "Comment Driven" module, allowing field changes on
      comment, eg. CaseTracker  [ http://drupal.org/project/driven ]

  # Views: atrium_leads
    - leads page, leads block, Exposed Search form, Browse by Tags Block 
       (Must allow Tags vocab to be used with Leads content type.)

  # Context: spaces-feature-leads

Development:
  This project is hosted on GitHub.  Feel free to contribute.
    [ http://github.com/thinkdrop/atrium_leads ]

Distribution:
  This project is hosted and distributed by ThinkDrop Consulting:
    http://features.thinkdrop.net/project/atrium_leads

--------------------------

