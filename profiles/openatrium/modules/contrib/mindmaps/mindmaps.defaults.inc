<?php

/**
 * Helper to implementation of hook_context_default_contexts().
 */
function _mindmaps_context_default_contexts() {
  $items = array();

  $items[] = array(
    'namespace' => 'spaces',
    'attribute' => 'feature',
    'value' => 'mindmaps',
    'description' => 'Mindmaps',
    'node' => array(
      '0' => 'graphmind',
    ),
    'menu_trail' => array(
      'mindmaps' => 'mindmaps',
    ),
    'menu' => 'mindmaps',
  );
  return $items;
}

/**
 * Helper to implementation of hook_menu_default_items().
 */
function _mindmaps_menu_default_items() {
  $items = array();

  $items[] = array(
    'title' => 'Mindmaps',
    'path' => 'mindmaps',
    'weight' => '0',
    'description' => 'Group\'s mindmaps',
  );
  // Translatables
  array(
    t('Group\'s mindmaps'),
    t('Mindmaps'),
  );


  return $items;
}

/**
 * Helper to implementation of hook_user_default_permissions().
 */
function _mindmaps_user_default_permissions() {
  $permissions = array();

  // Exported permission: access services
  $permissions[] = array(
    'name' => 'access services',
    'roles' => array(
      '0' => 'admin',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: administer services
  $permissions[] = array(
    'name' => 'administer services',
    'roles' => array(
      '0' => 'admin',
    ),
  );

  // Exported permission: create graphmind content
  $permissions[] = array(
    'name' => 'create graphmind content',
    'roles' => array(
      '0' => 'admin',
      '1' => 'authenticated user',
      '2' => 'manager',
    ),
  );

  // Exported permission: delete any graphmind content
  $permissions[] = array(
    'name' => 'delete any graphmind content',
    'roles' => array(
      '0' => 'admin',
    ),
  );

  // Exported permission: delete own graphmind content
  $permissions[] = array(
    'name' => 'delete own graphmind content',
    'roles' => array(
      '0' => 'admin',
      '1' => 'authenticated user',
      '2' => 'manager',
    ),
  );

  // Exported permission: edit any graphmind content
  $permissions[] = array(
    'name' => 'edit any graphmind content',
    'roles' => array(
      '0' => 'admin',
      '1' => 'authenticated user',
      '2' => 'manager',
    ),
  );

  // Exported permission: edit own graphmind content
  $permissions[] = array(
    'name' => 'edit own graphmind content',
    'roles' => array(
      '0' => 'admin',
      '1' => 'authenticated user',
      '2' => 'manager',
    ),
  );

  // Exported permission: get any user data
  $permissions[] = array(
    'name' => 'get any user data',
    'roles' => array(
      '0' => 'admin',
    ),
  );

  // Exported permission: get own user data
  $permissions[] = array(
    'name' => 'get own user data',
    'roles' => array(
      '0' => 'admin',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: load node data
  $permissions[] = array(
    'name' => 'load node data',
    'roles' => array(
      '0' => 'admin',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: update any user data
  $permissions[] = array(
    'name' => 'update any user data',
    'roles' => array(
      '0' => 'admin',
    ),
  );

  // Exported permission: update own user data
  $permissions[] = array(
    'name' => 'update own user data',
    'roles' => array(
      '0' => 'admin',
      '1' => 'authenticated user',
    ),
  );

  return $permissions;
}
