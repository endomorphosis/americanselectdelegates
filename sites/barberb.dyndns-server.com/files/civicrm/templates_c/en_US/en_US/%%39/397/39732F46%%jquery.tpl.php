<?php /* Smarty version 2.6.26, created on 2012-11-12 19:12:30
         compiled from CRM/common/jquery.tpl */ ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/jquery.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/jquery-ui.js"></script>
<style type="text/css">@import url("<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/themes/smoothness/jquery-ui.css");</style>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/flexigrid.js"></script>
<style type="text/css">@import url("<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/css/flexigrid.css");</style>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.autocomplete.js"></script>
<style type="text/css">@import url("<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/css/jquery.autocomplete.css");</style>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/tree_component.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/css.js"></script>
<style type="text/css">@import url("<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/css/tree_component.css");</style>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.menu.pack.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.dimensions.js"></script>
<style type="text/css">@import url("<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/css/menu.css");</style>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.chainedSelects.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.treeview.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.bgiframe.pack.js"></script>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.contextMenu.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.tableHeader.js"></script>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/checkboxselect.js"></script>

<?php if ($this->_tpl_vars['defaultWysiwygEditor'] == 1): ?>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/tinymce/jscripts/tiny_mce/jquery.tinymce.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<?php else: ?>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/ckeditor/ckeditor.js"></script>
<?php endif; ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.textarearesizer.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.progressbar.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.form.js"></script>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.tokeninput.js"></script>
<style type="text/css">@import url("<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/css/token-input-facebook.css");></style>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.timeentry.pack.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.mousewheel.pack.js"></script>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.toolTip.js"></script>

<style type="text/css">@import url("<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/css/dataTable.css");></style>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.dashboard.js"></script>
<style type="text/css">@import url(<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/css/dashboard.css);></style>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['config']->resourceBase; ?>
packages/jquery/plugins/jquery.FormNavigate.js"></script>

<script type="text/javascript">var cj = jQuery.noConflict(); $ = cj;</script>