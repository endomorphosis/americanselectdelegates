<?php
//  $Id: page-shell-popup.tpl.php,v 1.1 2010/04/18 15:36:50 richardp Exp $
/*
  This template file is used to display the popup of Shell.  Most of the extraneous styles and
  other content (like logo, sitename, etc) have been removed.  The point of this is to give
  Shell as much room as possible in the popup, and also to ensure that the javascript
  resizing functions are more likely to work accurately.
  
  You can override this file!  Just copy it into your theme directory and make whatever
  changes you wish to it.
*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body>

<div id="shell-popup-page">

        <div id="content-area">
          <?php print $content; ?>
        </div>

</div>
  
</body>
</html>