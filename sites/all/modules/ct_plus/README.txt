CONTENTS OF THIS FILE
---------------------
 * Overview
 * Installation
 * More information
 
OVERVIEW
--------

A feature-based version of Case Tracker, which relies much more heavily on
standard Drupal modules like CCK and Comment Driven, and is thus much more
extensible. Contains nearly 100% of the functionality of Case Tracker, and then
some, with Gantt charts, progress and duration fields, and task dependencies
out-of-the-box. Included submodules provide Open Atrium integration and
migration from Case Tracker and Storm environments.

This Feature provides two new content types (Task and Project) replete with CCK
fields such as "Progress", "Duration", etc... and several Views of tasks and
projects, including a Gantt chart. It styles the CCK fields on Tasks and
Projects in a way that can easily accommodate new custom fields, and allows
these fields to be updated via comments.

INSTALLATION
------------

Please see INSTALL.txt

MORE INFORMATION
----------------

Please see the project page at http://drupal.org/project/ct_plus