<?php

/**
 * @file
 * Implements required forms for rules conditions and actions.
 */

function ec_receipt_condition_receipt_status_form($settings, &$form) {
  $settings += array('status' => 0);

  $form['settings']['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#default_value' => $settings['status'],
    '#options' => ec_receipt_get_statuses(),
    '#description' => t('Select the status to compare'),
  );
}

