<?php

/**
 * @file apachesolr_storm_search.theme.inc
 * 
 * themeing functions for the apachesolr storm search
 * 
 */


/**
 * theme search form
 * 
 * @param unknown_type $form
 * @return unknown_type
 */
function theme_apachesolr_storm_search_form($form) {
  
  $content = '';
  
  $content .= '<div class="apachesolr-storm-search-form">';
  
  // KEYWORDS TEXTFIELD
  $content .= '<div class="apachesolr-storm-search-form-keys">';
  $content .= drupal_render($form['keys_sitewide']);
  $content .= '</div>';
  
  // SUGGESTIONS
  $content .= '<div class="apachesolr-storm-search-form-suggestion">';
  $content .= drupal_render($form['suggestion']);
  $content .= '</div>';
  
  // FILTERS
  $content .= '<div class="apachesolr-storm-search-form-filters">';
  $content .= drupal_render($form['filters']);
  $content .= '</div>';
  
  // SUBMIT BUTTON
  $content .= '<div class="apachesolr-storm-search-form-submit">';
  $content .= drupal_render($form['submit']);
  $content .= '</div>';
  
  // THEME THE REST OF THE FORM (!IMPORTANT!)
  $content .= drupal_render($form);
  
  
  $content .= '</div>'; // END <div class="apachesolr-storm-search-form">
  
  return $content;
}