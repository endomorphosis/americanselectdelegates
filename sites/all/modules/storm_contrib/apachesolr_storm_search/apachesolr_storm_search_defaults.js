/**
 * default values of input fields
 * @author Nikola Ivanov, nikola.ivanov@cocomore.com
 **/

var ASS = ASS || {};
ASS.defaults = function($) {
  var _config = [];
  
  /**
   * handle the focus event of an input field
   */
  var focusHandler = function() {
    var e = this;
    if ($(e).val() == $(e).data('JU.defaults')) {
      $(e).val('');
    }
  };
  
  /**
   * handle the blur event of an input field
   */  
  var blurHandler = function() {
    var e = this;
    if ($(e).val() == '') {
      $(e).val($(e).data('JU.defaults'));
    }
  };
  
  /**
   * handle the submit event of a form
   * @param DOMElement form element
   */  
  var submitHandler = function (fE) {
    if ($('#' + fE).length == 0) {
      return;
    }
    $('#' + fE).submit(function() {
      $('input:text, input:password', $('#' + fE)).each(function(i){
        if ($(this).data('JU.defaults') == $(this).val()) {
          $(this).val('');
        }
      });
    });
  };
  
  /**
   * public API
   */
  return {
    /**
     * constructor
     * @param {} options
     * @param DOMElement form element
     */
    init: function(o, fE) {
      var settings = {
        focusHandler: focusHandler,
        blurHandler: blurHandler,
        submitHandler: submitHandler,
        formElement: ''
      };
      var options = {};
      $.each(o, function (i, n) {
        if ($.isFunction(o[i])) {
          options[i] = n;
        }
      });
      options.formElement = fE;
      $.extend(settings, options);
      settings.submitHandler(settings.formElement);
      var id = _config.push(settings);
      id--;
      return id;
    },
    /**
     * set default value
     * @param int key in config array
     * @param DOMElement input field
     * @param mixed the value
     */
    set: function (cid, e, v) {
      $('#' + e).data('JU.defaults', v).val(v).focus(_config[cid].focusHandler).blur(_config[cid].blurHandler);
    },
    /**
     * get default value
     * @param DOMElement of input field
     */
    get: function (e) {
      return $('#' + e).data('JU.defaults');
    }
  };
}(jQuery);