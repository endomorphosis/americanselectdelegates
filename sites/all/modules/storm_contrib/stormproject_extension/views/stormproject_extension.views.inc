<?php

/**
 * @file stormproject_extension.views.inc
 */

/**
 * Implementation of hook_views_data()
 * 
 * @return unknown_type
 */
function stormproject_extension_views_data() {
  
  // --------------------------------------
  // TABLE STORMPROJECT EXTENSION BUDGETS
  // --------------------------------------
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['stormproject_extension_budgets']['table']['group']  = 'Storm';
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['stormproject_extension_budgets']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  
  // numeric text field budget costs
  $data['stormproject_extension_budgets']['budget_costs'] = array(
    'title' => t('Project Budget Costs'),
    'help' => t('The entered budget costs of a project.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // plain text field budget costs currency
  $data['stormproject_extension_budgets']['budget_costs_currency'] = array(
    'title' => t('Project Budget Costs Currency'),
    'help' => t('The selected currency of the budget costs (USD,EUR,...)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  
  // numeric text field budget duration
  $data['stormproject_extension_budgets']['budget_duration'] = array(
    'title' => t('Project Budget Duration'),
    'help' => t('The entered budget duration of a project.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  // plain text field budget duration unit
  $data['stormproject_extension_budgets']['budget_duration_unit'] = array(
    'title' => t('Project Budget Duration Unit'),
    'help' => t('The selected unit of the budget duration (hour/day,...).'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  
  
  // ----------------------------------------
  // TABLE STORMPROJECT EXTENSION DEADLINES
  // ----------------------------------------
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['stormproject_extension_deadlines']['table']['group']  = 'Storm';
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['stormproject_extension_deadlines']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // timestamp field deadline
  $data['stormproject_extension_deadlines']['deadline'] = array(
    'title' => t('Project Deadline'),
    'help' => t('The selected deadline of a project.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  return $data;
}

