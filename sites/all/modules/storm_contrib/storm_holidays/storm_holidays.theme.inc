<?php
// $Id: $

/**
 * @file storm_holidays.theme.inc
 *
 * theme functions for storm holidays
 */


/**
 * theme admin settings for storm holiday settings
 * @param unknown_type $form
 */
function theme_storm_holidays_admin_form_holiday_table($form) {
  $output = "";

  $header = array(t('Holiday'), $form['storm_holidays_enabled']['#title'], $form['storm_holidays_half_day']['#title']);

  unset($form['storm_holidays_enabled']['#title']);
  unset($form['storm_holidays_half_day']['#title']);

  $rows = array();
  foreach(element_children($form['storm_holidays_enabled']) as $key) {
    $row = array();

    $row[] = $form['storm_holidays_enabled'][$key]['#title'];

    unset($form['storm_holidays_enabled'][$key]['#title']);
    $row[] = drupal_render($form['storm_holidays_enabled'][$key]);
    unset($form['storm_holidays_half_day'][$key]['#title']);
    $row[] = drupal_render($form['storm_holidays_half_day'][$key]);

    $rows[] = $row;
  }

  $output .= theme('table', $header, $rows);

  $output .= drupal_render($form);

  return $output;
}