<?php

/**
 * @file stormperson_extension.module
 *
 * Modifications on storm persons
 *
 */


/**
 * implementation of hook_perm()
 *
 * @return array
 */
function stormperson_extension_perm() {
  return array('stormperson set hired till date', 'stormperson handover node');
}

/**
 * implementation of hook_theme()
 *
 */
function stormperson_extension_theme($existing, $type, $theme, $path) {
  return array(
    'stormperson_extension_page_view_adds' => array(
      'arguments' => array('node' => NULL),
      'file' => 'stormperson_extension.theme.inc',
    ),
    'stormperson_extension_not_work_anymore' => array(
      'arguments' => array('node' => NULL),
      'file' => 'stormperson_extension.theme.inc',
    ),
  );
}

/**
 * Implementation of hook_form_alter
 */
function stormperson_extension_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'stormperson_list_filter':
      // TODO extend the form with departments
      // but we need onchange handler on the organization dropdown
      // to refresh the departments list
      break;

      // PERSON NODE FORM
    case 'stormperson_node_form':

      // ----------------------------------------------
      // ADD EMPTY ITEM TO ORGANIZATION AND FORCE USER
      // TO SELECT A PROJECT IF NECESSARY
      // ----------------------------------------------
      storm_contrib_common_organization_add_empty_item($form);

      // Upload Picture / Avatar:
      if (!empty($form['#node']->user_uid)) {
        if (variable_get('user_pictures', 0) && $form['#node']->nid) {
          $account = (array) user_load(array('uid' => $form['#node']->user_uid));
          $form['#uid'] = $form['#node']->user_uid;
          $user_form = user_edit_form($form_state, $form['#uid'], $account, FALSE);
          $form['picture'] = $user_form['picture'];
          $form_state['values']['_account'] = $account;
          $form['#validate'] = array_merge($form['#validate'], $user_form['#validate']);
          $form['#submit'][] = "stormperson_extension_node_form_user_pictures_submit";
        }
      }

      if (user_access('stormperson set hired till date')) {
        $default_date = 0;
        if (!empty($form['#node']->hired_till)) {
          $default_date = storm_contrib_common_timestamp_to_timezone($form['#node']->hired_till);
        }

        $form['hired_till'] = array(
          '#type' => 'dateext',
          '#title' => t('Hired till'),
          '#default_value' => $default_date,
          '#withnull' => TRUE,
          '#description' => t('After the date selected the person, will not be considered anymore in !reports.', array('!reports' => l(t('Reports'), 'storm/reports'))),
        );

        if (variable_get('stormperson_extionsion_disable_user_after_quitting', FALSE)) {
          $form['hired_till']['#description'] .= '</br> '.t('User will be disabled, after selected date.');
        }

        if (!empty($form['#node']->hired_till)) {
          $form['hired_till_old'] = array('#type' => 'value', '#value' => $form['#node']->hired_till);
        }

        $form['#submit'][] = "stormperson_extension_node_form_hired_till_submit";
      }


      $form['#validate'][] = 'stormperson_extension_person_node_form_validate';
      $form['#pre_render'][] = 'stormperson_extension_pre_render_person_form';

      break;

  }
}

/**
 *
 * Handle upload of user avatar
 *
 *
 * @param unknown_type $form $form_state
 */
function stormperson_extension_node_form_user_pictures_submit($form, &$form_state) {
  $uid = $form['#node']->user_uid;
  $account = user_load($uid);
  // Delete picture if requested, and if no replacement picture was given.
  if (!empty($form_state['values']['picture_delete'])) {
    if ($account->picture && file_exists($account->picture)) {
      file_delete($account->picture);
    }
    $form_state['values']['picture'] = '';
    user_save($account, array('picture' => $form_state['values']['picture']));
  } else {
    // Finally save the changes
    if (!empty($form_state['values']['picture'])) {
      user_save($account, array('picture' => $form_state['values']['picture']));
    }
  }
}

/**
 * Handle hired till setup
 *
 * @param  $form
 * @param  $form_state
 * @return void
 */
function stormperson_extension_node_form_hired_till_submit($form, &$form_state) {
  if (!empty($form_state['values']['hired_till'])) {

    // IF NO DATE IS SELECTED (WITHOUT DATE POPUP)
    if (is_array($form_state['values']['hired_till']) && $form_state['values']['hired_till']['year'] <= 0) {
      return;
    }

    // CONVERT TO LINUX TIMESTAMP AND TO GM TIME
    $hired_till = storm_contrib_common_timestamp_to_gm(storm_contrib_common_date_to_time($form_state['values']['hired_till']));

    if (!empty($hired_till)) {
      $form_state['values']['hired_till'] = $hired_till;
      if (variable_get('stormperson_extionsion_disable_user_after_quitting', FALSE) && $hired_till < time()) {
        $uid = $form['#node']->user_uid;
        $account = user_load($uid);
        $account->status = 0;
        user_save($account, array('status' => 0));
        drupal_set_message(t('User %name disabled, because is not hired anymore.', array('%name' => $account->name)));
        $form_state['values']['status'] = 0;
      }
    }
  }
  // person rehired - value changed to empty or to a date in the future
  if ((empty($form_state['values']['hired_till']) && !empty($form_state['values']['hired_till_old'])) ||
    (!empty($hired_till) && $hired_till > time() && !empty($form_state['values']['hired_till_old']) && $form_state['values']['hired_till_old'] < $hired_till)) {

    $uid = $form['#node']->user_uid;
    $account = user_load($uid);
    if ($account->status == 0) {
      $account->status = 1;
      user_save($account, array('status' => 1));
    }
    drupal_set_message(t('User %name enabled, because is hired again.', array('%name' => $account->name)));
    $form_state['values']['status'] = 1;
  }

}

/**
 * pre render the task form before it is rendered as html
 * update the options of the select forms because they maybe were modified by javascript
 * but the form is fetched from the cache without the modifications
 *
 * @param unknown_type $form
 */
function stormperson_extension_pre_render_person_form($form) {

  // --------------------
  // CHECK ORGANIZATIONS
  // --------------------
  if (!empty($form['group1']['organization_nid']['#value']) && $form['group1']['organization_nid']['#value'] != $form['group1']['organization_nid']['#default_value']) {
    $form['group1']['organization_nid']['#default_value'] = $form['group1']['organization_nid']['#value'];
    // OPTIONS ARE THE SAME BECAUSE THEY ARE NOT CHANGED BY JS
  }

  return $form;
}


/**
 * validate stormperson node form
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @return unknown_type
 */
function stormperson_extension_person_node_form_validate($form, &$form_state) {

  // -------------------
  // CHECK ORGANIZATION
  // -------------------
  if (empty($form_state['values']['organization_nid']) || '' == $form_state['values']['organization_nid']) {
    form_set_error('organization_nid', t('You have to select an organization'));
  }

}


/**
 * Implementation of hook_menu().
 */
function stormperson_extension_menu() {

  $items['person'] = array(
    'title' => 'My Person',
    'page callback' => 'stormperson_extension_redirect_to_stormperson',
    'access callback' => 'stormperson_extension_redirect_to_stormperson_access',
    'type' => MENU_NORMAL_ITEM,
  );

  $items['node/%node/handover'] = array(
    'title' => 'Handover Node',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('stormperson_extension_handover_form', 1),
    'access callback' => 'stormperson_extension_handover_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
  );

  $items['admin/user/handover'] = array(
    'title' => 'Handover all Nodes from User',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('stormperson_extension_handover_form', NULL),
    'access arguments' => array('stormperson handover node'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['storm/person/autocomplete'] = array(
    'title' => 'Storm person autocomplete',
    'page callback' => 'stormperson_extension_autocomplete',
    'access callback' => 'user_access',
    'access arguments' => array('access user profiles'),
    'type' => MENU_CALLBACK,
  );

  $items['people/autocomplete'] = array(
    'title' => 'People autocomplete',
    'page callback' => 'people_autocomplete',
    'access callback' => 'user_access',
    'access arguments' => array('access user profiles'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for existing people.
 */
function people_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_query_range("SELECT fullname FROM {stormperson} WHERE fullname LIKE '%s%%'", $string, 0, 10);
    while ($person = db_fetch_object($result)) {
      $matches[$person->fullname] = check_plain($person->fullname);
    }
  }
  drupal_json($matches);
}

/**
 * check if redirect link to stormperson should be displayed
 *
 * @return unknown_type
 */
function stormperson_extension_redirect_to_stormperson_access() {
  global $user;
  // admin needs access to this redirect, even if he has no assigned stormperson
  // otherwise you can't see & move My Person in Navigation Menu, because admin has no access to it
  if ((!empty($user->stormperson_nid) && $user->stormperson_nid > 0) || $user->uid == 1) {
    return TRUE;
  }
  return FALSE;
}


/**
 * redirect to stormperson node
 *
 * @return unknown_type
 */
function stormperson_extension_redirect_to_stormperson() {
  global $user;
  // admin has access to this menu entry, but maybe didn't have a assigned person, so redirect him to the frontpage
  if (!empty($user->stormperson_nid) && $user->stormperson_nid != -1) {
    drupal_goto('node/'. $user->stormperson_nid);
  }
  else {
    drupal_goto('<front>');
  }
}


/**
 * Implementation of hook_nodeapi()
 *
 * @param unknown_type $node
 * @param unknown_type $op
 * @param unknown_type $a3
 * @param unknown_type $a4
 * @return unknown_type
 */
function stormperson_extension_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

  if ('stormperson' != $node->type) {
    return;
  }


  switch ($op) {

    case 'load':
      if (!empty($node->user_uid)) {
        if (empty($node->user_picture) || !isset($node->user_status)) {
          // LOAD USER STATUS AND PICTURE
          $person_user = user_load($node->user_uid);

          if (!empty($person_user)) {

            $node->user_status = $person_user->status;
            if (!empty($person_user->picture)) {
              $node->user_picture = $person_user->picture;
            }

            // Join date / since
            $just_date = str_replace(array('H:i', 'g:ia', ' - '), '', variable_get('date_format_short', 'm/d/Y - H:i'));
            $node->joined = format_date($person_user->created, 'custom', $just_date);
            $node->joined_ago = format_interval(time() - $person_user->created);

            // Online status
            $node->last_active = format_interval(time() - $person_user->access);
            $image_path = drupal_get_path('module', 'stormperson_extension') . '/images';

            if (round((time()-$person_user->access)/60) < 15) {
              $node->online_icon = theme('image', "$image_path/user-online.png", t('User is online'), t('User is online'));
              $node->online_status = t('Online');
            }
            else {
              $node->online_icon = theme('image', "$image_path/user-offline.png", t('User offline. Last seen @time ago.', array('@time' => $node->last_active)), t('User offline. Last seen @time ago.', array('@time' => $node->last_active)));
              $node->online_status = t('Offline');
            }

          }

          unset($person_user);
        }
      }
      stormperson_extension_load_extension($node);
      break;

    case 'insert':
    case 'update':
      stormperson_extension_save_extension($node, $op);
      break;

    case 'delete':
      stormperson_extension_delete_extension($node);
      break;

    case 'delete revision':
      stormperson_extension_delete_extension($node, $node->vid);
      break;

    case 'view':

      if ('stormperson' == $node->type) {

        $node->content['stormperson_extension_add_ons'] = array(
        '#value' => stormperson_extension_page_view_adds($node),
        '#weight' => -3,
        );
        $now = storm_contrib_common_timestamp_to_gm(time());
        if (!empty($node->hired_till) && $node->hired_till < $now) {
          $node->content['stormperson_extension_not_work_anymore'] = array(
            '#value' => theme('stormperson_extension_not_work_anymore', $node),
            '#weight' => -20,
          );
        }

      }

      break;

  }

}

/**
 * load stormperson_extension to node
 *
 * @param  $node
 * @return void
 */
function stormperson_extension_load_extension(&$node) {
  $sql = 'SELECT spx.hired_till FROM {stormperson_extension} spx WHERE spx.nid = %d AND spx.vid = %d';
  $args = array($node->nid, $node->vid);
  $sql = db_rewrite_sql($sql, 'spx', 'nid', $args);
  $result = db_query($sql, $args);
  if (!empty($result)) {
    while($row = db_fetch_array($result)) {
      foreach ($row as $key => $value) {
        $node->$key = $value;
      }
    }
  }
}

/**
 * save person extension fields to database
 *
 * @param  $node
 * @param  $op
 * @return void
 */
function stormperson_extension_save_extension(&$node, $op) {

  $object = new stdClass();
  $object->nid = $node->nid;
  $object->vid = $node->vid;

  // TIMEZONE HAS BEEN REMOVED IN SUBMIT FUNCTION!
  $object->hired_till = storm_contrib_common_date_to_time($node->hired_till);

  $update = array();
  if ("insert" != $op && !empty($node->nid) && !empty($node->vid)) {
    $count = db_result(db_query("SELECT COUNT(nid) FROM {stormperson_extension} WHERE nid = %d AND vid = %d", $node->nid, $node->vid));
    if ($count > 0) {
      $update = array('nid', 'vid');
    }
  }

  drupal_write_record('stormperson_extension', $object, $update);
}

/**
 * delete person extension entries from database. if is set vid, deletes only this revision
 *
 * @param  $node
 * @param null $vid
 * @return void
 */
function stormperson_extension_delete_extension($node, $vid = NULL) {
  if (!empty($node->nid)) {

    $args = array();
    $sql  = 'DELETE FROM {stormperson_extension} WHERE ';
    $sql .= 'nid = %d ';
    $args[] = $node->nid;

    if (!empty($vid)) {
      $sql .= 'AND vid = %d ';
      $args[] = $vid;
    }

    db_query(db_rewrite_sql($sql, 'spx', 'nid', $args), $args);

  }
}

/**
 * page view add ons
 *
 * @param object $node
 * @return string
 *   themed HTML output
 */
function stormperson_extension_page_view_adds($node) {
  $content = '';

  if (!empty($node)) {

    // GET TEAMS OF USER OF PERSON
    $node->teams = array();
    if (!empty($node->user_uid) && module_exists('stormteam')) {
      $person_user = user_load($node->user_uid);
      if (!empty($person_user)) {
        // GET TEAMS
        $teams = stormteam_user_return_teams($person_user);
        foreach($teams as $nid) {
          $team_node = node_load($nid);
          if (!empty($team_node)) {
            $node->teams[$nid] = $team_node->title;
          }
        }
      }
    }

    // DEPARTEMENTS
    $node->department_terms = storm_contrib_common_get_node_taxonomy($node, storm_contrib_common_get_taxonomy_department());

    // SKILLS
    $node->skills = storm_contrib_common_get_node_taxonomy($node, storm_contrib_common_get_taxonomy_skills());

    $content = theme('stormperson_extension_page_view_adds', $node);
  }

  return $content;
}


/**
 * Implementation of hook_user()
 *
 * @param unknown_type $op
 * @param unknown_type $edit
 * @param unknown_type $account
 * @param unknown_type $category
 * @return unknown_type
 */
function stormperson_extension_user($op, &$edit, &$account, $category = NULL) {

  switch ($op) {

    // VIEW
    case 'view':

      if (!empty($account->stormperson_nid) && is_numeric($account->stormperson_nid)) {
        $node_person = node_load($account->stormperson_nid);
        if (!empty($node_person)) {
          $account->content['stormticket_assignment'] = array(
          '#type' => 'user_profile_category',
          '#title' => t('Assigned Person'),
          'assigned_person' => array(
            '#type' => 'user_profile_item',
            '#title' => '',
            '#value' => l($node_person->title, 'node/'. $node_person->nid),
            ),
          );
        }
      }
      unset($node_person);

      break;

  }

}


/**
 * Implementation of hook_clone_node_alter() from the node_clone module
 *
 * @param unknown_type $node
 * @param unknown_type $original_node
 * @param unknown_type $op
 * @return unknown_type
 */
function stormperson_extension_clone_node_alter($node, $original_node, $op) {

  if (!empty($node) && !empty($original_node)) {

    switch ($op) {

      case 'prepopulate':

        storm_contrib_common_set_active_trail('storm/peoples');

        $node->organization_nid = $original_node->organization_nid;
        $node->prefix = $original_node->prefix;
        $node->fullname = $original_node->fullname;
        $node->email = $original_node->email;
        $node->www = $original_node->www;
        $node->phone = $original_node->phone;
        $node->im = $original_node->im;

        break;

    }

  }
}

function stormperson_extension_storm_contrib_common_admin_settings_form($form) {

  // ---------------------------
  // DEADLINE SETTINGS
  // ---------------------------
  $form['stormperson_extension'] = array(
  '#type' => 'fieldset',
  '#title' => t('Person Settings'),
  '#collapsible' => TRUE,
  '#collapsed' => TRUE,
  '#weight' => 0,
  );

  $form['stormperson_extension']['stormperson_extionsion_disable_user_after_quitting'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable user, after Person quites'),
    '#description' => t('Set user status to 0 (disabled), after persons \'hired till\' date.'),
    '#default_value' => variable_get('stormperson_extionsion_disable_user_after_quitting', FALSE),
  );

  $form['stormperson_extension']['stormperson_extension_hide_teams'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide teams in persons view'),
    '#description' => t('If checked, teams will be hidden in persons view'),
    '#default_value' => variable_get('stormperson_extension_hide_teams', FALSE),
  );
  
  $form['stormperson_extension']['handover_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Handover Node Types'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => 0,
  );

  $node_types = node_get_types('names');

  $form['stormperson_extension']['handover_types']['stormperson_extension_handover_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('This node types will be hand over to the new author, if you hand over a complete !user', array('!user' => l(t('user'), 'user/handover'))),
    '#description' => t(''),
    '#options' => $node_types,
    '#default_value' => variable_get('stormperson_extension_handover_types', array('stormticket', 'stormtask', 'storminvoice', 'stormexpense', 'stormnote', 'stormproject', 'stormteam')),
  );

  return $form;

}

function stormperson_extension_cron() {
  if (variable_get('stormperson_extionsion_disable_user_after_quitting', FALSE)) {
    $time = storm_contrib_common_timestamp_to_gm(time());
    $result = db_query('SELECT u.uid, sp.nid, sp.vid FROM {stormperson_extension} spx JOIN {stormperson} sp USING (nid, vid) JOIN {users} u ON u.uid = sp.user_uid  WHERE hired_till < %d AND status != %d AND hired_till > 0', $time, 0);
    while ($row = db_fetch_array($result)) {
      $account = user_load($row['uid']);
      $account->status = 0;
      user_save($account, array('status' => 0));
      $node = node_load($row['nid'], $row['vid']);
      $node->status = 1;
      node_save($node);
    }
  }
}

function stormperson_extension_handover_form($form_state, $node = NULL) {
  $form = array();

  if (!empty($node->uid)) {
    $form['handover_from'] = array(
      '#type' => 'hidden',
      '#value' => $node->uid,
    );
  }
  else {
    $form['handover_from'] = array(
      '#type' => 'textfield',
      '#title' => t('Handover from user id'),
      '#description' => t('Use autocomplete to retrive Userid'),
      '#autocomplete_path' => 'storm/person/autocomplete',
      '#required' => TRUE,
    );
  }

  $form['handover_to'] = array(
    '#type' => 'textfield',
    '#title' => t('Handover to user id'),
    '#description' => t('Use autocomplete to retrive Userid'),
    '#autocomplete_path' => 'storm/person/autocomplete',
    '#required' => TRUE,
  );

  $form['handover_check'] = array(
    '#type' => 'checkbox',
    '#title' => t('Are you sure, to handover this node to the new user?'),
  );

  if (!empty($node->nid)) {
    $form['handover_nid'] = array(
      '#type' => 'hidden',
      '#value' => $node->nid,
    );
    $account = user_load($node->uid);
    $form['handover_check']['#description'] = t('This will change the author and project manager from project %name and all the tickets, tasks, invoice, expense, notes, team from this Project created by %author to the new selected author.',
      array('%name' => $node->title, '%author' => $account->name));
  }
  else {
    $form['handover_nid'] = array(
      '#type' => 'hidden',
      '#value' => 'ALL',
    );
    $form['handover_check']['#description'] = t('This will change the author and project manager from all projects, task, tickets, ... to the new user.');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

function stormperson_extension_handover_form_validate($form, &$form_state) {
  if ($form_state['values']['handover_check'] == 0) {
    form_set_error('handover_check', t('You must check the checkbox to handover the project.'));
  }

  if (!is_numeric($form_state['values']['handover_from']) || $form_state['values']['handover_from']<=0) {
    form_set_error('handover_from', t('You must enter a valid UserId in the \'handover from\' field'));
  }
  else {
    $account = user_load($form_state['values']['handover_from']);
    if (!is_object($account)) {
      form_set_error('handover_from', t('You must enter a valid UserId in the \'handover from\' field'));
    }
    else {
      $form_state['storage']['handover_from_account'] = $account;
    }
  }

  if (!is_numeric($form_state['values']['handover_to']) || $form_state['values']['handover_to']<=0) {
    form_set_error('handover_to', t('You must enter a valid UserId in the \'handover to\' field'));
  }
  else {
    $account = user_load($form_state['values']['handover_to']);
    if (!is_object($account)) {
      form_set_error('handover_to', t('You must enter a valid UserId in the \'handover to\' field'));
    }
    else {
      $form_state['storage']['handover_to_account'] = $account;
    }
  }

}

function stormperson_extension_handover_form_submit($form, &$form_state) {
  $nid = $form_state['values']['handover_nid'];
  $from = $form_state['storage']['handover_from_account'];
  $to = $form_state['storage']['handover_to_account'];

  $tables = array();
  if (module_exists('stormticket')) {
    $tables[] = 'stormticket';
  }
  if (module_exists('stormtask')) {
    $tables[] = 'stormtask';
  }
  if (module_exists('storminvoice')) {
    $tables[] = 'storminvoice';
  }
  if (module_exists('stormexpense')) {
    $tables[] = 'stormexpense';
  }
  if (module_exists('stormnote')) {
    $tables[] = 'stormnote';
  }

  if ($nid !== 'ALL' && is_numeric($nid)) {
    $nids = array($nid);

    // get all tickets, tasks, invoice, expense, notes from the project
    foreach ($tables as $table) {
      $result = db_query('SELECT nid FROM {%s} WHERE project_nid = %d', $table, $nid);
      while ($row = db_fetch_array($result)) {
        $nids[] = $row['nid'];
      }
    }
    if(module_exists('stormproject') && module_exists('stormteam')) {
     // get the assigend team
      $result = db_query('SELECT n.nid FROM {stormproject} sp JOIN {node} n ON (sp.assigned_nid = n.nid) WHERE sp.nid = %d AND n.type = \'%s\'', $nid, 'stormteam');
      while ($row = db_fetch_array($result)) {
        $nids[] = $row['nid'];
      }
    }
    // get other nids from other modules
    $temp_nids = module_invoke_all('stormperson_extension_handover_project', $nid, $from, $to);
    if (is_array($temp_nids)) {
      $nids = array_merge($nids, $temp_nids);
    }
    unset($temp_nids);

    //UPDATE AUTHOR
    db_query("UPDATE {node} SET uid = %d WHERE uid = %d AND nid IN (%s)", $to->uid, $from->uid, implode(',', $nids));

    //UPDATE PROJECTMANAGAER
    if (!empty($to->stormperson_nid) && module_exists('stormproject')) {
      $new_manager = node_load($to->stormperson_nid);
      db_query("UPDATE {stormproject} SET manager_nid = %d, manager_title = '%s' WHERE nid = %d", $to->stormperson_nid, $new_manager->title, $nid);
    }

  }
  else {

    if (module_exists('stormproject')) {
      $tables[] = 'stormproject';
    }
    if (module_exists('stormteam')) {
      $tables[] = 'stormteam';
    }

    // in this case table name == type name
    $types = variable_get('stormperson_extension_handover_types', $tables);

    // give other modules the chance to change their manager or something like that
    // types added in admin settings
    module_invoke_all('stormperson_extension_handover_all', $from, $to);

    foreach ($types as $key => $type) {
      if (empty($type)) {
        unset($types[$key]);
      }
      else {
        $types[$key] = db_escape_string($type);
      }
    }

    //UPDATE AUTHOR
    db_query("UPDATE {node} SET uid = %d WHERE uid = %d AND type IN ('".implode("','", $types)."')", $to->uid, $from->uid);

    //UPDATE PROJECTMANAGAER
    if (!empty($to->stormperson_nid) && !empty($from->stormperson_nid) && module_exists('stormproject')) {
      $new_manager = node_load($to->stormperson_nid);
      db_query("UPDATE {stormproject} SET manager_nid = %d, manager_title = '%s' WHERE manager_nid = %d", $to->stormperson_nid, $new_manager->title, $from->stormperson_nid);
    }

  }

  drupal_set_message(t('Author and Projectmanager changed to !name', array('!name' => $to->name)));

}

function stormperson_extension_handover_access($node) {
  if ("stormproject" == $node->type && user_access('stormperson handover node')) {
    return TRUE;
  }
  return FALSE;
}

function stormperson_extension_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_query_range("SELECT user_uid, fullname FROM {stormperson} WHERE fullname LIKE '%%%s%%'", $string, 0, 10);
    while ($user = db_fetch_object($result)) {
      $matches[$user->user_uid] = check_plain($user->fullname);
    }
    if (10-count($matches) > 0) {
      $result = db_query_range("SELECT uid, name FROM {users} WHERE name LIKE '%s%%'", $string, 0, 10-count($matches));
      while ($user = db_fetch_object($result)) {
        $matches[$user->uid] = check_plain($user->name);
      }
    }
  }

  drupal_json($matches);
}
