
Storm Contrib
============

Name: storm_contrib
Author: Carsten Müller | Cocomore AG
Drupal: 6.x
Sponsor: Cocomore AG - http://www.cocomore.com


About
=====

Storm Contrib is a package of add on modules for the storm project (http://www.drupal.org/project/storm). 
It offers extensions and new features to the storm modules like calculation of costs, consideration of holidays 
and weekends, timetracking statistics and project balance informations. For details see the details for each module


Requirements
============
- the storm project is needed (http://www.drupal.org/project/storm)


Installation
============

1. Place whole storm_contrib folder into your contrib Drupal modules directory.
   (/sites/all/modules/contrib)

2. Enable the desired storm_contrib modules by navigating to
     administer > modules (URL: admin/build/modules)

3. Configure the storm admin settings
     administer > settings > storm
     Within the storm settings there is a tab for the storm_contrib settings

4. If needed you can extend the taxonomy vocabulary of your departments 

5. Now there a several new features and modifications available within storm 
    depending on the modules you enabled in step 2



Documentation
=============


apachesolr_storm_search
-----------------------
Features:
- search quickly for storm content types with the apachesolr search

An extension for the apache solr search integration module (http://www.drupal.org/project/apachesolr)
It offers a block to search with apache solr just for storm content types like organizations, projects, tasks,
tickets or persons. Very useful if you have solr running on your system and you quickly want to find a storm item.


storm_contrib_common
--------------------
Features:
- common functions for all storm_contrib modules

Offers common functions for the other storm_contrib modules. 
Needed by nearly every other module within storm_contrib. 


storm_contrib_costs
-------------------
Features:
- calculation of costs
- hourly or daily cost rate of each person
- monthly salary of each person
- calculation of a project cost balance
- calculation of costs of each person by timetrackings
- calculation of estimated costs by using the entered duration
- comparison to an optional project budget
- project balance ratios

The storm_contrib_costs module offers calculation of costs within storm for projects, organizations, tasks, tickets and persons. 
For each person a cost rate (by day or by hour) can be entered. The system uses these informations to calculate the 
estimated costs of a project, task or ticket by selecting the duration and the costs of the assigned persons. 
It also calculates the real costs by using the timetrackings a user has entered. For each project a budget can be entered. 
The system creates a project balance by comparing the estimated and the real costs with the durations of the entered budget. 
So a project manager can easily see the current status of his project. If desired a nightly status mail can be send to 
each project manager displaying the current status of the project (module stormproject_extension needed).
For each person there could be also a monthly salary entered. Then the system calculates the real costs per hour for this person by 
selecting the whole duration the person tracked on timetrackings in the month and divides the monthly salary through this duration.

storm_contrib_log
-----------------
Features:
- adds Log/Activity API for certain storm events
- uses heartbeat or storm_contrib_logger

The storm_contrib_log module logs following errors and activities: storm node added or updated, missing timetrackings for a person,
exceeded utilisation of a person, taks/tickets with no person or department assigned, project costs exceeded and project deadline reached.
All messages are customizable and translatable by heatbeat or storm_contrib_logger and can be disabled.
You have to select one activity log module. Currently there are a implementation for heartbeat and a small self written module.
Heartbeat provides more features for theming, alteration and adds Rules and View support. storm_contrib_logger is a very small module, when you only want
to view the messages, with as less overhead as possible.

storm_contrib_logger
--------------------
storm_contrib_logger module implements storm_contrib_log api, when you do not want the overhead of heatbeat.

storm_dashboard_modification
----------------------------
Features:
- enlarges the storm dashboard

Modifications on the storm_dashboard module like enlargements of the width and height so more information can be seen on the dashboard.


storm_dependencies
------------------
Features:
- defines dependencies between tickets and tasks

Offers the possibility the define dependencies between tickets and tasks. Sometimes a ticket must first be finished before another
ticket can be started. Therefore this module is needed.


stormexpense_extension
----------------------
Extensions and modifications on the stormexpense module. Calculates the total costs of expenses for a project.


storm_exports
-------------
Features:
- export of lists

Offers the possibility to export lists as .csv, .xls, .doc, .txt or .xml


storm_extension
---------------
Extensions and modifications on the storm module. 
The storm dashboard is overwritten to offer the possibility for other modules to add their icons to the dashboard.


storm_holidays
--------------
Features:
- consideration of weekends and national holidays

Offers the possibility to consider weekends and national holidays within storm. 
The system will automatically consider these days when calculating the end date of a ticket, task or project.
For example if you enter a new ticket and just select a start date and enter a duration the system will automatically
calculate the end date of this ticket considering national holidays and weekends. 

storm_filter
---------------
Features:
- save search filter options

Offers the possibility to save your currently active search filter.
Saved filters can be access via the 'Storm Filter Bookmark' block.
Also you can mark saved filters as global and/or default, so that anyone can use the filter or it will be automatically
 loaded on viewing the list.
Works currently with all Lists provided from storm and can be extended via hook_storm_contrib_filter_active_forms -
 at the moment only for filters saved in the user session.

storminvoice_extension
----------------------
Extensions and modifications on the storminvoice module. Calculates the total amount of invoices for a project.


stormorganization_extension
---------------------------
Extensions and modifications on the stormorganization module.


stormperson_extension
---------------------
Extensions and modifications on the stormperson module.


stormperson_holidays
--------------------
Features:
- consideration of person holidays

Offers the possibility to enter holidays for each person. These holidays will be considered within storm.


stormperson_utilisation
-----------------------
Features:
- displays the utilisation of each person

Offers the possibilty to display the utilisation of a person on each day. The assigned tickets and tasks
will be displayed on the page of the person and the utilisation in percentage will be calculated. Therefore
for each person the working time per day has to be entered. If the charts module with the google charts in enabled
a chart of the persons utilisation will be displayed. If a person gets too many tasks or tickets a warning will be displayed.


stormproject_extension
----------------------
Extensions and modifications on the stormproject module.


stormtask_extension
-------------------
Extensions and modifications on the stormtask module.


stormticket_assigment
---------------------
Module to offer the possibility to easy assign tickets or tasks to persons and to move these
tickets or tasks in the time. If enabled it is easy to move tickets and tasks.


stormticket_assigment_jsapp
---------------------------


stormticket_extension
---------------------
Extensions and modifications on the stormticket module.


stormtimetracking_extension
---------------------------
Extensions and modifications on the stormtimetracking module.


stormtimetracking_favorites
---------------------------
Features:
- list of favorite tickets, tasks or projects for easy timetracking

Offers the possibility for each person the create a list of favorite tickets, tasks, projects or organizations.
The list displays all items ordered by organization and project and displays for each item the quick timetracker clock.
So each person can easily start and stop timetracking on items by just clicking in ths list. No clicking through the
whole project is needed any more. Very usefull especially if you have many projects at the same time and many phone
calls where you hav to switch quickly between the projects. 
