/**
 * @author nikola.ivanov@cocomore.com
 * @copy Cocomore AG
 * 
 * tickets assignment magic
 * 
 * explicit usage of jQuery() instead of $() because of jQuery.noConflict()
 * explicit usage of ready() instead of Drupal behaviours to avoid conflict with $
 */

jQuery(document).ready(function($) {

  $("input.stormperson-tickets").val('');
  
  $("ul.storm-tickets-connectedSortables")
    .not('.storm-tickets-list')
    .bind('storm-tickets-update', function() {
      // this = <ul>
      
      var data, el;
      data = $.map(jQuery(this).sortable('toArray'), 
                        function(el) { 
                          el = el.match((/(.+)[-=_](.+)/)); 
                          return el[2]; 
                        }
      );
      el = $(this).parents('div.person-wrapper');
      if (el) {
        $('#stormperson-tickets-'+ $(el).attr('id')).val(data.join(','));
      }
    })
    .end()
    .sortable({
      connectWith: '.storm-tickets-connectedSortables',
      update: function (event, ui) {
        if (ui.sender == null) {
          $(this).trigger('storm-tickets-update'); 
        }
      },
      receive: function (event, ui) {
        // this = <li>
        $(this).trigger('storm-tickets-update');
      }
    })
    .disableSelection();
});