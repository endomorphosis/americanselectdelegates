<?php

/**
 * @file stormticket_assignment.theme.inc
 *
 * theming functions for module stormticket_assignment
 */


/**
 * theme the link
 *
 * @param unknown_type $linktitle
 * @param unknown_type $linktarget
 * @param unknown_type $class
 * @return unknown_type
 */
function theme_stormticket_assignment_link($linktitle, $linktarget, $class) {
  $content  = '';
  $content .= '<div class="'. $class .'">';
  $content .= l($linktitle, $linktarget);
  $content .= '</div>';
  return $content;
}

/**
 * theme user ticket list
 */
function theme_stormticket_assignment_user_tickets($result) {

  global $user;

  drupal_add_css(drupal_get_path('module', 'stormticket_assignment') .'/stormticket_assignment.css');

  $output = '';

  $header = $result['header'];

  $output .= '<div class="user-tickets-wrapper">';

  $output .= $result['filter'];

  if (!empty($result['tickets'])) {
    foreach ($result['tickets'] as $key => $ticket_array) {

      $class_datebegin = '';
      $class_dateend = '';

      $n = new stdClass();
      $n->nid = $ticket_array['nid'];
      $n->uid = $user->uid;
      $n->organization_nid = $ticket_array['organization_nid'];
      $n->type = 'stormticket';

      $node = node_load($n->nid);

      // IF DATE BEGIN == TODAY
      if (!empty($ticket_array['ticket_today_datebegin'])) {
        $class_datebegin = 'ticket-today-datebegin';
      }

      // IF DATE END == TODAY
      if (!empty($ticket_array['ticket_today_dateend'])) {
        $class_dateend = 'ticket-today-dateend';
      }

      // IF DATE BEGIN IS OVERDUE
      if (!empty($ticket_array['ticket_overdue_datebegin'])) {
        $class_datebegin = 'ticket-overdue-datebegin';
      }

      // IF DATE END IS OVERDUE
      if (!empty($ticket_array['ticket_overdue_dateend'])) {
        $class_dateend = 'ticket-overdue-dateend';
      }

      $row = array();
      $row[] = array('data' => $ticket_array['nid']);
      $row[] = array('data' => l($ticket_array['title'], 'node/'. $ticket_array['nid']));
      $row[] = array('data' => storm_icon('status_'. $ticket_array['ticketstatus'], storm_attribute_value('Ticket status', $ticket_array['ticketstatus'])));
      $row[] = array('data' => storm_icon('category_'. $ticket_array['ticketcategory'], storm_attribute_value('Ticket category', $ticket_array['ticketcategory'])));
      $row[] = array('data' => storm_icon('priority_'. $ticket_array['ticketpriority'], storm_attribute_value('Ticket priority', $ticket_array['ticketpriority'])));

      if (!empty($ticket_array['datebegin'])) {
        $row[] = array('data' => storm_contrib_common_time_to_date($ticket_array['datebegin'], storm_contrib_common_get_custom_display_date_format()), 'class' => $class_datebegin);
      }
      else {
        $row[] = array('data' => ' ');
      }
      if (!empty($ticket_array['dateend'])) {
        $row[] = array('data' => storm_contrib_common_time_to_date($ticket_array['dateend'], storm_contrib_common_get_custom_display_date_format()) , 'class' => $class_dateend);
      }
      else {
        $row[] = array('data' => ' ', 'class' => $class_dateend);
      }

      $row[] = array('data' => theme('storm_contrib_common_format_plural_duration', $ticket_array['duration'], $ticket_array['durationunit']));

      $row[] = array('data' => l($ticket_array['assigned_title'], 'node/'. $ticket_array['assigned_nid']));
      $row[] = array('data' => l($ticket_array['task_title'], 'node/'. $ticket_array['task_nid']));
      $row[] = array('data' => l($ticket_array['project_title'], 'node/'. $ticket_array['project_nid']));
      $row[] = array('data' => l($ticket_array['organization_title'], 'node/'. $ticket_array['organization_nid']));
      $tmp = array('data' => '');

      if ( node_access('update', $node) || (1 == $user->uid) ) {
        $tmp['data'] =  storm_icon_edit_node($n, $_GET);
        if ( node_access('delete', $node) || (1 == $user->uid) ) {
          $tmp['data'] .= '&nbsp;'. storm_icon_delete_node($n, $_GET);
          $tmp['class'] = 'storm_list_operations';
        }
        $row[] = $tmp;
      }
      else {
        $row[] = $tmp;
      }
      
	    // QUICK TIMETRACKING
	    if (module_exists('storm_dashboard')) {
	      $row[] = array('data' => _storm_dashboard_timetracking_trigger(TRUE, $ticket_array['nid']));
	    }
      
      $rows[] = array('data' => $row);
    }

  }
  else {
    $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '13'));
  }

  $output .= theme('table', $header, $rows, array('id' => 'ticket-overview'));
  if (!empty($result['pager'])) {
    $output .= $result['pager'];
  }

  // EXPORT
  if (module_exists('storm_exports')) {

    if (!empty($result['export'])) {
      $output .= $result['export'];
    }

    // IF EXPORT BUTTON WAS PRESSED
    if (!empty($_SESSION['storm_exports_export_'. request_uri() ])) {
      $vars['filename'] = 'my-tickets';
      $vars['header'] = $header;
      $vars['rows'] = $rows;
      storm_exports_export($vars);
    }

  }

  $output .= '</div>';
  
  return $output;
}


/**
 * theme user ticket list block
 */
function theme_stormticket_assignment_block_user_tickets($result) {

  global $user;

  drupal_add_css(drupal_get_path('module', 'stormticket_assignment') .'/stormticket_assignment.css');

  $output = '';

  $header = $result['header'];

  $output .= '<div class="user-tickets-wrapper">';

  if (!empty($result['tickets'])) {
    foreach ($result['tickets'] as $key => $ticket_array) {

      $class_datebegin = '';
      $class_dateend = '';

      $n = new stdClass();
      $n->nid = $ticket_array['nid'];
      $n->uid = $user->uid;
      $n->organization_nid = $ticket_array['organization_nid'];
      $n->type = 'stormticket';

      $node = node_load($n->nid);

      // IF DATE BEGIN == TODAY
      if (!empty($ticket_array['ticket_today_datebegin'])) {
        $class_datebegin = 'ticket-today-datebegin';
      }

      // IF DATE END == TODAY
      if (!empty($ticket_array['ticket_today_dateend'])) {
        $class_dateend = 'ticket-today-dateend';
      }

      // IF DATE BEGIN IS OVERDUE
      if (!empty($ticket_array['ticket_overdue_datebegin'])) {
        $class_datebegin = 'ticket-overdue-datebegin';
      }

      // IF DATE END IS OVERDUE
      if (!empty($ticket_array['ticket_overdue_dateend'])) {
        $class_dateend = 'ticket-overdue-dateend';
      }

      $row = array();
      $row[] = array('data' => storm_icon('priority_'. $ticket_array['ticketpriority'], storm_attribute_value('Ticket priority', $ticket_array['ticketpriority'])));
      $row[] = array('data' => $ticket_array['nid']);
      $row[] = array('data' => l($ticket_array['title'], 'node/'. $ticket_array['nid']));
      $row[] = array('data' => l($ticket_array['project_title'], 'node/'. $ticket_array['project_nid']));
      $row[] = array('data' => l($ticket_array['organization_title'], 'node/'. $ticket_array['organization_nid']));
      
      if (!empty($ticket_array['datebegin'])) {
        $row[] = array('data' => storm_contrib_common_time_to_date($ticket_array['datebegin'], storm_contrib_common_get_custom_display_date_format()), 'class' => $class_datebegin);
      }
      else {
        $row[] = array('data' => ' ');
      }
      if (!empty($ticket_array['dateend'])) {
        $row[] = array('data' => storm_contrib_common_time_to_date($ticket_array['dateend'], storm_contrib_common_get_custom_display_date_format()) , 'class' => $class_dateend);
      }
      else {
        $row[] = array('data' => ' ', 'class' => $class_dateend);
      }

      $tmp = '<table width="100%" border=0><tr><td style="text-align:right; padding-right:0px" class="stormticket-assignment-duration">'.theme('storm_contrib_common_number_format', $ticket_array['duration']).'</td>';
      $tmp.= '<td style="text-align:left; padding-left:4px" class="stormticket-assignment-durationunit">'.t(substr($ticket_array['durationunit'],0,1)).'</td></tr></table>';
      $row[] = $tmp;
      
      if (node_access('update', $node) || (1 == $user->uid)) {
        $row[] = array('data' => storm_icon_edit_node($n, $_GET));
      }
      else {
        $row[] = array('data' => '', 'style' => 'width:1px;padding:0');
      }
      
      // QUICK TIMETRACKING
      if (module_exists('storm_dashboard')) {
        $row[] = array('data' => _storm_dashboard_timetracking_trigger(TRUE, $node->nid));
      }
      $rows[] = array('data' => $row);
    }

  }
  else {
    $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '13'));
  }

  $output .= theme('table', $header, $rows, array('id' => 'ticket-overview'));
  if (!empty($result['pager'])) {
    $output .= $result['pager'];
  }

  // EXPORT
  if (module_exists('storm_exports')) {

    if (!empty($result['export'])) {
      $output .= $result['export'];
    }

    // IF EXPORT BUTTON WAS PRESSED
    if (!empty($_SESSION['storm_exports_export_'. request_uri() ])) {
      $vars['filename'] = 'my-tickets';
      $vars['header'] = $header;
      $vars['rows'] = $rows;
      storm_exports_export($vars);
    }

  }

  $output .= '</div>';
  
  return $output;
}

/**
 * theme user ticket list
 */
function theme_stormticket_assignment_user_tasks($result) {

  global $user;

  drupal_add_css(drupal_get_path('module', 'stormticket_assignment') .'/stormticket_assignment.css');

  $output = '';

  $header = $result['header'];

  $output .= '<div class="user-tasks-wrapper">';

  $output .= $result['filter'];


  if (!empty($result['tasks'])) {
    foreach ($result['tasks'] as $key => $task_array) {

      $class_datebegin = '';
      $class_dateend = '';

      $n = new stdClass();
      $n->nid = $task_array['nid'];
      $n->uid = $user->uid;
      $n->organization_nid = $task_array['organization_nid'];
      $n->type = 'stormtask';

      $node = node_load($n->nid);

      // IF DATE BEGIN == TODAY
      if (!empty($task_array['task_today_datebegin'])) {
        $class_datebegin = 'task-today-datebegin';
      }

      // IF DATE END == TODAY
      if (!empty($task_array['task_today_dateend'])) {
        $class_dateend = 'task-today-dateend';
      }

      // IF DATE BEGIN IS OVERDUE
      if (!empty($task_array['task_overdue_datebegin'])) {
        $class_datebegin = 'task-overdue-datebegin';
      }

      // IF DATE END IS OVERDUE
      if (!empty($task_array['task_overdue_dateend'])) {
        $class_dateend = 'task-overdue-dateend';
      }

      $row = array();
      $row[] = array('data' => $task_array['nid']);
      $row[] = array('data' => l($task_array['title'], 'node/'. $task_array['nid']));
      $row[] = array('data' => storm_icon('status_'. $task_array['taskstatus'], storm_attribute_value('Task status', $task_array['taskstatus'])));
      $row[] = array('data' => storm_icon('category_'. $task_array['taskcategory'], storm_attribute_value('Task category', $task_array['taskcategory'])));
      $row[] = array('data' => storm_icon('priority_'. $task_array['taskpriority'], storm_attribute_value('Task priority', $task_array['taskpriority'])));
      $row[] = array('data' => storm_contrib_common_time_to_date($task_array['datebegin'], storm_contrib_common_get_custom_display_date_format()), 'class' => $class_datebegin);
      $row[] = array('data' => storm_contrib_common_time_to_date($task_array['dateend'], storm_contrib_common_get_custom_display_date_format()), 'class' => $class_dateend);
      $row[] = array('data' => theme('storm_contrib_common_format_plural_duration', $task_array['duration'], $task_array['durationunit']));
      $row[] = array('data' => l($task_array['assigned_title'], 'node/'. $task_array['assigned_nid']));
      $row[] = array('data' => l($task_array['project_title'], 'node/'. $task_array['project_nid']));
      $row[] = array('data' => l($task_array['organization_title'], 'node/'. $task_array['organization_nid']));
      $tmp = array('data' => '');

      if ( node_access('update', $node) || (1 == $user->uid) ) {
        $tmp['data'] =  storm_icon_edit_node($n, $_GET);
        if ( node_access('delete', $node) || (1 == $user->uid) ) {
          $tmp['data'] .= '&nbsp;'. storm_icon_delete_node($n, $_GET);
          $tmp['class'] = 'storm_list_operations';
        }
        $row[] = $tmp;
      }
      else {
        $row[] = $tmp;
      }
      $rows[] = array('data' => $row);
    }

  }
  else {
    $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '12'));
  }

  $output .= theme('table', $header, $rows, array('id' => 'task-overview'));
  if (!empty($result['pager'])) {
    $output .= $result['pager'];
  }

  // EXPORT
  if (module_exists('storm_exports')) {

    if (!empty($result['export'])) {
      $output .= $result['export'];
    }

    // IF EXPORT BUTTON WAS PRESSED
    if (!empty($_SESSION['storm_exports_export_'. request_uri() ])) {
      $vars['filename'] = 'my-tasks';
      $vars['header'] = $header;
      $vars['rows'] = $rows;
      storm_exports_export($vars);
    }

  }

  $output .= '</div>';

  return $output;
}

/**
 * theme a assignment overdue mail
 *
 * @param $node_assignment Ticket/Task node
 * @param $mail_type plain/html
 * @return string themed output
 */
function theme_stormticket_assignment_overdue_assignments($node_assignment, $mail_type) {
  $content = '';

  switch ($mail_type) {

    case 'plain':
      $content .= $node_assignment->nid .' - '. $node_assignment->title . ' - '. t('ended') .': '. storm_contrib_common_time_to_date($node_assignment->dateend, storm_contrib_common_get_custom_display_date_format() ) . ' ('. str_replace('storm', '', $node_assignment->type) .')';
      break;

    case 'html':
      $content .= $node_assignment->nid .' - '. l($node_assignment->title, 'node/'. $node_assignment->nid, array('absolute' => TRUE)) . ' - '. t('ended') .': '. storm_contrib_common_time_to_date($node_assignment->dateend, storm_contrib_common_get_custom_display_date_format() ) . ' ('. str_replace('storm', '', $node_assignment->type) .')';
      break;

  }

  return $content;
}


/**
 * theme project schedule
 */
function theme_stormticket_assignment_project_schedule($result) {

  global $user;

  if (!empty($result['node_project'])) {
    $node_project = $result['node_project'];
  }

  drupal_add_css(drupal_get_path('module', 'stormticket_assignment') .'/stormticket_assignment.css');

  $output = '';
  $rows_export = array();

  $type = $result['type'];
  $header = $result['header'];
  $filters = $result['filters'];

  $output .= '<div id="project-schedule-wrapper">';

  $output .= $result['filter'];


  if (!empty($result['projects'])) {
    foreach ($result['projects'] as $project_nid => $project_array) {

      if ( (!empty($project_nid) || 0 == $project_nid) && is_numeric($project_nid)) {

        $output .= '<p>&nbsp;</p>';

        $output .= '<h2>';
        $output .= l($project_array['title']." (".$project_nid.")", 'node/'. $project_nid);
        $output .= '</h2>';

        $rows = array();

        if (!empty($project_array['tasks'])) {
          foreach ($project_array['tasks'] as $task_nid => $task_array) {

            if ( (!empty($task_nid) || 0 == $task_nid) && is_numeric($task_nid)) {

              // -----------
              // EMPTY LINE IF EXPORT
              // -----------
              if (!empty( $_SESSION['storm_exports_export_'. request_uri() ]) && !empty($rows)) {
                $row = array();
                $row[] = array('data' => ' ', 'colspan' => 13);
                $rows[] = array('data' => $row);
              }


              // -------
              // TASKS
              // -------
              $class_datebegin = '';
              $class_dateend = '';

              $n = new stdClass();
              $n->nid = $task_nid;
              $n->uid = $user->uid;
              $n->type = 'stormtask';

              $node_task = node_load($n->nid);

              // IF DATE BEGIN == TODAY
              if (!empty($task_array['task_today_datebegin'])) {
                $class_datebegin = 'ticket-today-datebegin';
              }

              // IF DATE END == TODAY
              if (!empty($task_array['task_today_dateend'])) {
                $class_dateend = 'ticket-today-dateend';
              }

              // IF DATE BEGIN IS OVERDUE
              if (!empty($task_array['task_overdue_datebegin'])) {
                $class_datebegin = 'ticket-overdue-datebegin';
              }

              // IF DATE END IS OVERDUE
              if (!empty($task_array['task_overdue_dateend'])) {
                $class_dateend = 'ticket-overdue-dateend';
              }


              // IF NOT EXPORT
              if (empty($_SESSION['storm_exports_export_'. request_uri() ])) {
                if (!empty($task_array['task_title']) && !empty($task_array['task_nid'])) {
                  $task_array['task_title'] = '<strong>'. l($task_array['task_title'], 'node/'. $task_array['task_nid']) .'</strong> ';
                }
              }

              $row = array();
              $row[] = $task_nid;

              // TASK TITLE
              if (!empty($task_array['depends_on'])) {
                $row[] = array('data' => $task_array['task_title'] .' ('. t('Task') .')<br><div id="storm-dependencies-depends-on"><b>'.t('Depends on: !depends_on', array('!depends_on' => l($task_array['depends_on']['title'], $task_array['depends_on']['parent_nid']))).'</b></div>');
              } else {
                $row[] = array('data' => $task_array['task_title'] .' ('. t('Task') .')');
              }

              // TASK DATEBEGIN
              if (!empty($task_array['datebegin'])) {
                $row[] = array('data' => storm_contrib_common_time_to_date($task_array['datebegin'], storm_contrib_common_get_custom_display_date_format()), 'class' => $class_datebegin);
              }
              else {
                $row[] = array('data' => ' ');
              }

              // TASK DATEEND
              if (!empty($task_array['dateend'])) {
                $row[] = array('data' =>  storm_contrib_common_time_to_date($task_array['dateend'], storm_contrib_common_get_custom_display_date_format()), 'class' => $class_dateend);
              }
              else {
                $row[] = array('data' => ' ');
              }


              // DURATION
              if (!is_null($task_array['duration']) && !empty($task_array['durationunit'])) {
                $row[] = array('data' => theme('storm_contrib_common_format_plural_duration', $task_array['duration'], $task_array['durationunit']));
              }
              else {
                $row[] = array('data' => theme('storm_contrib_common_format_plural_duration', 0, 'hour'));
              }

              // TASK TIME TRACKED
              if (!is_null($task_array['time_tracked']) && !empty($task_array['durationunit'])) {
                $row[] = array('data' => theme('storm_contrib_common_format_plural_duration', $task_array['time_tracked'], $task_array['durationunit']));
              }
              else {
                $row[] = array('data' => theme('storm_contrib_common_format_plural_duration', 0, 'hour'));
              }

              // TASK PROGRESS
              if (!is_null($task_array['progress'])) {
                $row[] = array('data' => theme('storm_contrib_common_progress_bar', $task_array['progress'], $task_array['duration']));
              }
              else {
                $row[] = array('data' => ' ');
              }

              // TASK ASSIGNMENT
              if (empty($task_array['assigned_title']) || ' ' == $task_array['assigned_title']) {
                $row[] = array('data' => t('unassigned'), 'class' => 'unassigned');
              }
              else {
                $row[] = array('data' => l($task_array['assigned_title'], 'node/'. $task_array['assigned_nid']));
              }


              // IF EXPORT
              if (!empty( $_SESSION['storm_exports_export_'. request_uri() ])) {
                // TASK STATUS
                $row[] = array('data' => storm_attribute_value('Task status', $task_array['taskstatus']));

                // TASK CATEGORY
                $row[] = array('data' => storm_attribute_value('Task category', $task_array['taskcategory']));

                // TASK PRIORITY
                $row[] = array('data' => storm_attribute_value('Task priority', $task_array['taskpriority']));
              }
              else {
                // TASK STATUS
                $row[] = array('data' => storm_icon('status_'. $task_array['taskstatus'], storm_attribute_value('Task status', $task_array['taskstatus'])));

                // TASK CATEGORY
                $row[] = array('data' => storm_icon('category_'. $task_array['taskcategory'], storm_attribute_value('Task category', $task_array['taskcategory'])));

                // TASK PRIORITY
                $row[] = array('data' => storm_icon('priority_'. $task_array['taskpriority'], storm_attribute_value('Task priority', $task_array['taskpriority'])));
              }

              // TASK LAST MODIFIED
              if (!empty($task_array['last_modified'])) {
                $row[] = array('data' => format_date($task_array['last_modified'], 'small'));
              }
              else {
                $row[] = array('data' => ' ');
              }

              $tmp = array('data' => '');

              // TIMETRACKING TRIGGER ICON
              if (module_exists('storm_dashboard') && ( !empty($task_array['datebegin']) ||!empty($task_array['dateend']) ) ) {
                $tmp['data'] .= _storm_dashboard_timetracking_trigger(TRUE, $node_task->nid) .' ';
              }

              // EDIT ICON
              if ( node_access('update', $node_task) || (1 == $user->uid) && ( !empty($task_array['datebegin']) ||!empty($task_array['dateend']) ) ) {
                $tmp['data'] .=  storm_icon_edit_node($n, $_GET);
              }

              // DELETE ICON
              if ( node_access('delete', $node_task) || (1 == $user->uid) && ( !empty($task_array['datebegin']) ||!empty($task_array['dateend']) ) ) {
                $tmp['data'] .= '&nbsp;'. storm_icon_delete_node($n, $_GET);
                $tmp['class'] = 'storm_list_operations';
              }

              $row[] = $tmp;

              $rows[] = array('data' => $row);

              unset($n);
              unset($node_task);


              // --------
              // TICKETS
              // --------
              if (!empty($task_array['tickets'])) {
                foreach ($task_array['tickets'] as $ticket_nid => $ticket_array) {

                  if ( (!empty($ticket_nid) || 0 == $ticket_nid) && is_numeric($ticket_nid)) {

                    $class_datebegin = '';
                    $class_dateend = '';

                    $n = new stdClass();
                    $n->nid = $ticket_nid;
                    $n->uid = $user->uid;
                    $n->type = 'stormticket';

                    $node_ticket = node_load($n->nid);

                    // IF DATE BEGIN == TODAY
                    if (!empty($ticket_array['ticket_today_datebegin'])) {
                      $class_datebegin = 'ticket-today-datebegin';
                    }

                    // IF DATE END == TODAY
                    if (!empty($ticket_array['ticket_today_dateend'])) {
                      $class_dateend = 'ticket-today-dateend';
                    }

                    // IF DATE BEGIN IS OVERDUE
                    if (!empty($ticket_array['ticket_overdue_datebegin'])) {
                      $class_datebegin = 'ticket-overdue-datebegin';
                    }

                    // IF DATE END IS OVERDUE
                    if (!empty($ticket_array['ticket_overdue_dateend'])) {
                      $class_dateend = 'ticket-overdue-dateend';
                    }

                    $row = array();
                    $row[] = $ticket_nid;

                    // TICKET TITLE
                    if (!empty($ticket_array['depends_on'])) {
                      $ticket_array['ticket_title'] = '<ul class="project-shedule-stormticket"><li>'. l($ticket_array['ticket_title'], 'node/'. $ticket_array['ticket_nid']) .' ('. t('Ticket') .')' .'</li><ul><div id="storm-dependencies-depends-on"><b>'.t('Depends on: !depends_on', array('!depends_on' => l($ticket_array['depends_on']['title'], 'node/'. $ticket_array['depends_on']['parent_nid']))).'</b></div></ul></ul>';
                    } else {
                      $ticket_array['ticket_title'] = '<ul class="project-shedule-stormticket"><li>'. l($ticket_array['ticket_title'], 'node/'. $ticket_array['ticket_nid']) .' ('. t('Ticket') .')' .'</li></ul>';
                    }
                    $row[] = array('data' => $ticket_array['ticket_title']);

                    // TICKET DATEBEGIN
                    if (!empty($ticket_array['datebegin'])) {
                      $row[] = array('data' => storm_contrib_common_time_to_date($ticket_array['datebegin'], storm_contrib_common_get_custom_display_date_format()), 'class' => $class_datebegin);
                    }
                    else {
                      $row[] = array('data' => ' ');
                    }

                    // TICKET DATEEND
                    if (!empty($ticket_array['dateend'])) {
                      $row[] = array('data' => storm_contrib_common_time_to_date($ticket_array['dateend'], storm_contrib_common_get_custom_display_date_format()), 'class' => $class_dateend);
                    }
                    else {
                      $row[] = array('data' => ' ');
                    }

                    // TICKET DURATION
                    $row[] = array('data' => theme('storm_contrib_common_format_plural_duration', $ticket_array['duration'], $ticket_array['durationunit']));

                    // TICKET TIME TRACKED
                    $row[] = array('data' => theme('storm_contrib_common_format_plural_duration', $ticket_array['time_tracked'], $ticket_array['durationunit']));

                    // TICKET PROGRESS
                    $row[] = array('data' => theme('storm_contrib_common_progress_bar', $ticket_array['progress'], $ticket_array['duration']));

                    // TICKET ASSIGNMENT
                    if (empty($ticket_array['assigned_title']['#value']) || ' ' == $ticket_array['assigned_title']['#value']) {
                      $row[] = array('data' => t('unassigned'), 'class' => 'unassigned');
                    }
                    else {
                      $row[] = array('data' => l($ticket_array['assigned_title'], 'node/'. $ticket_array['assigned_nid']));
                    }

                    // IF EXPORT
                    if (!empty( $_SESSION['storm_exports_export_'. request_uri() ])) {
                      $row[] = array('data' => storm_attribute_value('Ticket status', $ticket_array['ticketstatus']));
                      $row[] = array('data' => storm_attribute_value('Ticket category', $ticket_array['ticketcategory']));
                      $row[] = array('data' => storm_attribute_value('Ticket priority', $ticket_array['ticketpriority']));
                    }
                    else {
                      $row[] = array('data' => storm_icon('status_'. $ticket_array['ticketstatus'], storm_attribute_value('Ticket status', $ticket_array['ticketstatus'])));
                      $row[] = array('data' => storm_icon('category_'. $ticket_array['ticketcategory'], storm_attribute_value('Ticket category', $ticket_array['ticketcategory'])));
                      $row[] = array('data' => storm_icon('priority_'. $ticket_array['ticketpriority'], storm_attribute_value('Ticket priority', $ticket_array['ticketpriority'])));
                    }

                    // TASK LAST MODIFIED
                    if (!empty($ticket_array['last_modified'])) {
                      $row[] = array('data' => format_date($ticket_array['last_modified'], 'small'));
                    }
                    else {
                      $row[] = array('data' => ' ');
                    }


                    $tmp = array('data' => '');

                    // TIMETRACKING TRIGGER ICON
                    if (module_exists('storm_dashboard')) {
                      $tmp['data'] .= _storm_dashboard_timetracking_trigger(TRUE, $node_ticket->nid) .' ';
                    }

                    // EDIT ICON
                    if ( node_access('update', $node_ticket) || (1 == $user->uid) ) {
                      $tmp['data'] .=  storm_icon_edit_node($n, $_GET);
                    }

                    // DELETE ICON
                    if ( node_access('delete', $node_ticket) || (1 == $user->uid) ) {
                      $tmp['data'] .= '&nbsp;'. storm_icon_delete_node($n, $_GET);
                      $tmp['class'] = 'storm_list_operations';
                    }

                    $row[] = $tmp;

                    $rows[] = array('data' => $row);

                    unset($n);
                    unset($node_ticket);

                  }

                }
              }

            }

          }

          if (!empty($project_array['duration_total_in_hours'])) {

            // EMPTY ROW
            // IF EXPORT
            if (!empty( $_SESSION['storm_exports_export_'. request_uri() ])) {
              $row = array();
              $row[] = array('data' => ' ');
              $rows[] = array('data' => $row);
            }

            $row = array();
            $row[] = array('data' => '<strong>'. t('Total') .'</strong>');
            if ($project_array['datebegin_total']['#value'] > time()) {
              $class_datebegin = 'ticket-overdue-datebegin';
            }
            $row[] = array('data' => '<strong>'. storm_contrib_common_time_to_date($project_array['datebegin_total'], storm_contrib_common_get_custom_display_date_format()) .'</strong>', 'class' => $class_datebegin);

            if ($project_array['dateend_total']['#value'] < time()) {
              $class_dateend = 'ticket-overdue-dateend';
            }
            $row[] = array('data' => '<strong>'. storm_contrib_common_time_to_date($project_array['dateend_total'], storm_contrib_common_get_custom_display_date_format()) .'</strong>', 'class' => $class_dateend);

            $row[] = array('data' => '<strong>'. theme('storm_contrib_common_format_plural_duration', storm_contrib_common_convert_durations($project_array['duration_total_in_hours'], 'hour', 'day', 'work_day'), 'day') .'</strong>');

            $row[] = array('data' => '<strong>'. theme('storm_contrib_common_format_plural_duration', storm_contrib_common_convert_durations($project_array['time_tracked_total_in_hours'], 'hour', 'day', 'work_day'), 'day') .'</strong>');

            $row[] = array('data' => '<strong>'. theme('storm_contrib_common_progress_bar', $project_array['total_progress']) .'</strong>', 'colspan' => 7);

            $rows[] = array('data' => $row);
          }

        }
        else {
          $rows[] = array(array('data' => t('No items available.'), 'colspan' => '13'));
        }

        if (!empty($_SESSION['storm_exports_export_'. request_uri() ])) {
          $row_tmp = array();
          $row_tmp[] = array('data' => ' ');
          $rows_export[] = array('data' => $row_tmp);
          $row_tmp = array();
          $row_tmp[] = array('data' => ' ');
          $rows_export[] = array('data' => $row_tmp);
          $row_tmp = array();
          $row_tmp[] = array('data' => ' ');
          $rows_export[] = array('data' => $row_tmp);
          $row_tmp = array();
          $row_tmp[] = array('data' => $project_nid);
          $row_tmp[] = array('data' => t('Project: @name', array('@name' => $project_array['title'])));
          $rows_export[] = array('data' => $row_tmp);
          $rows_export = array_merge($rows_export, $rows);
        }
        else {
          $output .= theme('table', $header, $rows, array('id' => 'project-schedule'));
        }

      }

    }
  }
  else {
    $output .= t('No items available.');
  }


  if ($result['pager']) {
    $output .= $result['pager'];
  }





  // EXPORT
  if (module_exists('storm_exports')) {

    if (!empty($result['export'])) {
      $output .= $result['export'];
    }

    // IF EXPORT BUTTON WAS PRESSED
    if (!empty($_SESSION['storm_exports_export_'. request_uri() ])) {
      $vars['filename'] = 'project-schedule';
      $vars['header'] = $header;
      unset($vars['header']['op']);
      $vars['rows'] = $rows_export;
      storm_exports_export($vars);
    }

  }

  $output .= '</div>';

  return $output;
}

function theme_stormticket_assignment_block_project_schedule($result) {
  global $user;
  $content = "";

  $rows = array();
  $access_costs = false;
  $access_deadline = false;
  if (module_exists('stormproject_extension')) {
    $access_deadline = user_access('view project deadline');
  }

  foreach ($result['projects'] as $nid => $node_project) {
    stormproject_extension_page_view_adds($node_project);

    $table_row = array();
    $table_row['nid'] = array('data' => $node_project->nid);
    $table_row['title'] = array('data' => l($node_project->title, 'node/'.$node_project->nid));
    $table_row['organization'] = array('data' => l($node_project->organization_title, 'node/'.$node_project->organization_nid));
    if ($access_deadline) {
      $table_row['deadline'] = array('data' => storm_contrib_common_time_to_date($node_project->project_deadline_time, storm_contrib_common_get_custom_display_date_format()));
    }
    else {
      $table_row['deadline'] = array('data' => storm_contrib_common_time_to_date($node_project->dateend, storm_contrib_common_get_custom_display_date_format()));
    }
    if (!empty($node_project->access_costs) && module_exists('storm_contrib_costs')) {
      $table_row['profit'] = array('data' => theme('storm_contrib_common_number_format', $node_project->profit) .' '. $node_project->profit_currency);
      $access_costs = true;
    }

    // IF BUDGET DURATION AVAILABLE
    if (!empty($node_project->budget_duration)) {
      $duration = $node_project->budget_duration;
      $duration_unit = $node_project->budget_duration_unit;
      $node_project->progress_type = 'budget_duration';
    }
    // ELSE USE ENTERED DURATION
    else {
      $duration = $node_project->duration;
      $duration_unit = $node_project->durationunit;
      $node_project->progress_type = 'duration';
    }

    $node_project->progress_in_percentage = 0;
    // STILL OPEN DURATIONS + TIMETRACKING DURATION TILL NOW
    if (!empty($node_project->still_open_total_real_duration)) {
      $timetracking_duration_in_hours = storm_contrib_common_convert_durations($node_project->still_open_total_real_duration, $duration_unit, 'hour', 'work_day');

      // CONVERT ENTERED DURATION INTO HOURS
      $duration_in_hours = storm_contrib_common_convert_durations($duration, $duration_unit, 'hour', 'work_day');

      if (!empty($duration_in_hours)) {
        $node_project->progress_in_percentage = ($timetracking_duration_in_hours / $duration_in_hours ) * 100;
      }

    }

    if (t('completed') == $node_project->projectstatus && $node_project->progress_in_percentage < 100) {
      $node_project->progress_in_percentage = 100;
    }

    $table_row['progress_in_percentage_value'] = array('data' => theme('storm_contrib_common_number_format', $node_project->progress_in_percentage).'%');
    $table_row['progress_in_percentage_bar'] = array('data' => theme('storm_contrib_common_progress_bar_pure', $node_project->progress_in_percentage, $duration, 100, 12), 'style' => 'width:110px;');

    if (node_access('update', $node_project) || (1 == $user->uid)) {
      $table_row[] = array('data' => storm_icon_edit_node($node_project, $_GET));
    }
    else {
      $table_row[] = array('data' => '', 'style' => 'width:1px;padding:0');
    }
    
    $rows[] = array('data' => $table_row);
  }
  if(empty($rows)) {
    $rows[] = array(array('data' => t('No posts available.'), 'colspan' => 8));
  }

  if (!$access_costs) {
    unset($result['header']['profit']);
  }

  $content .= theme('table', $result['header'], $rows);

  if(empty($rows)) {
    $content .= $result['pager'];
  }

  return $content;
}

/**
 * theme node revisions in a fieldset
 *
 * @param unknown_type $revisions
 */
function theme_stormticket_assignment_display_node_revisions($node, $revisions) {
  $content = "";

  if (!empty($node) && !empty($revisions)) {

    $header = array();
    $rows = array();

    foreach ($revisions as $revision) {
      $row = array();
      $operations = array();

      if ($revision->current_vid > 0) {
        $row[] = array('data' => t('!date by !username', array('!date' => l(format_date($revision->timestamp, 'small'), "node/". $node->nid), '!username' => theme('username', $revision)))
        . (($revision->log != '') ? '<p class="revision-log">'. filter_xss($revision->log, array('a', 'em', 'strong', 'cite', 'code', 'ul', 'ol', 'li', 'dl', 'dt', 'dd', 'br')) .'</p>' : ''),
                       'class' => 'revision-current');
      }
      else {
        $row[] = t('!date by !username', array('!date' => l(format_date($revision->timestamp, 'small'), "node/". $node->nid ."/revisions/". $revision->vid ."/view"), '!username' => theme('username', $revision)))
        . (($revision->log != '') ? '<p class="revision-log">'. filter_xss($revision->log, array('a', 'em', 'strong', 'cite', 'code', 'ul', 'ol', 'li', 'dl', 'dt', 'dd', 'br')) .'</p>' : '');
      }
      $rows[] = $row;
    }

    $element = array(
    '#type' => 'fieldset',
    '#title' => t('Assignment History'),
    '#value' => theme('table', $header, $rows),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    );

    $content = theme('fieldset', $element);

  }

  return $content;
}