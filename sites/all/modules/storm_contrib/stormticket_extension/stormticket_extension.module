<?php

/**
 * @file stormticket_extension.module
 *
 * Modifications on the stormticket module
 *
 */


/**
 * implementation of hook_theme()
 *
 */
function stormticket_extension_theme($existing, $type, $theme, $path) {
  return array(
    'stormticket_extension_ticket_page_view_adds' => array(
      'arguments' => array('node' => NULL),
      'file' => 'stormticket_extension.theme.inc',
    ),
    'stormticket_extension_ticket_page_view_timetracking_trigger' => array(
      'arguments' => array('node' => NULL),
      'file' => 'stormticket_extension.theme.inc',
    ),
  );
}


/**
 * Implementation of hook_form_alter()
 */
function stormticket_extension_form_alter(&$form, $form_state, $form_id) {

   switch ($form_id) {

    case 'stormticket_node_form':

      // ----------------------------------------------
      // ADD EMPTY ITEM TO ORGANIZATION AND FORCE USER
      // TO SELECT A PROJECT IF NECESSARY
      // ----------------------------------------------
      storm_contrib_common_organization_add_empty_item($form);

      // ------------------------
      // SET PROJECT AS REQUIRED
      // ------------------------
      if (!empty($form['group1']['project_nid']['#options'])) {
        if (variable_get('stormproject_extension_ticket_project_mandantory', FALSE)) {
          $form['group1']['project_nid']['#required'] = true;
        }
      }

      $request_uri = request_uri();

      // --------------
      // FIX END DATE
      // --------------
      // TODO REMOVE IF FIXED BY STORMTICKET MODULE ITSELF
      if ('1970-01-01' == $form['group3']['dateend']['#default_value'] && strpos($request_uri, '/node/add/stormticket') !== FALSE ) {
        // SET DEFAULT END DATE
        $form['group3']['dateend']['#default_value'] = date($form['group3']['dateend']['#date_format']);
      }

      if (empty($form['group3']['dateend']['#default_value']) && strpos($request_uri, '/node/add/stormticket') !== FALSE ) {
        // SET DEFAULT BEGIN AS END DATE
        $form['group3']['dateend']['#default_value'] = $form['group3']['datebegin']['#default_value'];
      }
      
      // -------------------
      // RECURRING DURATION
      // -------------------
      $default_weight = -17.5;
      if (module_exists('content')) {
        $weight = content_extra_field_weight($form['#node']->type, 'group3.5');
      }
      if (empty($weight)) {
        $weight = $default_weight;
      }

      $form['group3.5'] = array(
        '#type' => 'markup',
        '#theme' => 'storm_form_group',
        '#weight' => $weight,
      );

      $default = 0;
      if (!empty($form['#node']->is_recurring_duration)) {
        $default = $form['#node']->is_recurring_duration;
      }

      $form['group3.5']['is_recurring_duration'] = array(
      '#type' => 'checkbox',
      '#title' => 'Recurring duration',
      '#description' => 'If checked the entered duration will be calculated as recurring durations on every day of the selected time period',
      '#default_value' => $form['#node']->is_recurring_duration,
      );

      $form['#validate'][] = 'stormticket_extension_ticket_node_form_validate';
      $form['#pre_render'][] = 'stormticket_extension_pre_render_ticket_form';

      break;

    case 'stormticket_list_filter':

      // WORKAROUND FOR THE stormticket_list_filter() FUNCTION
      // IF OPTIONS ARE NOT SET
      // TODO REMOVE WORKAROUND IF PATCH IS SUBMITTED IN THE stormticket.admin.inc
      // see #852590 - http://drupal.org/node/852590
      $hit = FALSE;

      $organization_nid = $_SESSION['stormticket_list_filter']['organization_nid'];
      if (empty($organization_nid) && !empty($form_state['post']['organization_nid'])) {
        $_SESSION['stormticket_list_filter']['organization_nid'] = $form_state['post']['organization_nid'];
        $hit = TRUE;
      }

      $project_nid = $_SESSION['stormticket_list_filter']['project_nid'];
      if (empty($project_nid) && !empty($form_state['post']['project_nid'])) {
        $_SESSION['stormticket_list_filter']['project_nid'] = $form_state['post']['project_nid'];
        $hit = TRUE;
      }

      $task_nid = $_SESSION['stormticket_list_filter']['task_nid'];
      if (empty($task_nid) && !empty($form_state['post']['task_nid'])) {
        $_SESSION['stormticket_list_filter']['task_nid'] = $form_state['post']['task_nid'];
        $hit = TRUE;
      }

      if ($hit) {
        $filter_form = stormticket_list_filter($form_state);
        $form['filter'] = $filter_form['filter'];
      }

      break;

    case 'stormtimetracking_node_form':
      if (!empty($form['#node']->organization_nid) && !empty($form['#node']->project_nid) && !empty($form['#node']->task_nid)) {
        $tickets = stormticket_extension_get_tickets($form['#node']->organization_nid, $form['#node']->project_nid, $form['#node']->task_nid);
        $form['group1']['ticket_nid']['#options'] = array(0 => '-') + $tickets;
      }
      break;

    case 'stormtimetracking_list_filter':
      if (!empty($form['filter']['group1']['organization_nid']['#default_value']) && !empty($form['filter']['group1']['project_nid']['#default_value']) && !empty($form['filter']['group1']['task_nid']['#default_value'])) {
        $tickets = stormticket_extension_get_tickets($form['filter']['group1']['organization_nid']['#default_value'], $form['filter']['group1']['project_nid']['#default_value'], $form['filter']['group1']['task_nid']['#default_value']);
        $form['filter']['group1']['ticket_nid']['#options'] = array(0 => '-') + $tickets;
      }
      break;


   }

}

/**
 * implements hook_perm
 *
 * @return array
 */
function stormticket_extension_perm() {
  return array(
    'Storm ticket: view if project is assigned to team',
  );
}

/**
 * implements hook_storm_contrib_node_access (extended node_access)
 *
 * @param  $op
 * @param  $node
 * @param  $account
 * @param  $node_access
 * @return bool
 */
function stormticket_extension_storm_contrib_node_access($op, $node, $account, $node_access) {
  // we only want to allow more access to some nodes, so when we have already access, we do not need to do something
  if($node_access) {
    return $node_access;
  }

  if (module_exists('stormteam') && 'view'== $op && !empty($account) && user_access('Storm ticket: view if project is assigned to team', $account)) {
    // get all team members from the project
    $team_member = storm_contrib_common_get_stormteam_members_from_project($node->project_nid);

    if (!empty($team_member[$account->stormperson_nid])) {
      return TRUE;
    }
  }
  return $node_access;
}
/**
 * Implementation of hook_nodeapi()
 * @return unknown_type
 */
function stormticket_extension_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

  if ('stormticket' != $node->type) {
    return;
  }

  switch ($op) {

    case 'validate':
      // CHECK DURATION AND END DATE
      if ('completed' != $node->ticketstatus) {
        stormticket_extension_check_ticket_duration($node, $op, 'work_day');
      }
      break;

    case 'load':
      // LOAD RECURRING DURATION
      stormticket_extension_load_recurring_duration($node);
      break;

    case 'presave':
      // SET END DATE BY DURATION
      stormticket_extension_set_date_end_by_duration($node, 'work_day');
      break;

    case 'insert':
    case 'update':
      // SAVE RECURRING DURATION
      stormticket_extension_save_recurring_duration($node);
      break;

    case 'view':

      // GET FURTHER INFORMATIONS FOR TICKET PAGE
      stormticket_extension_page_view_adds($node);

      // DISPLAY FURTHER INFORMATIONS
      $node->content['stormticket_extension_add_ons'] = theme('stormticket_extension_ticket_page_view_adds', $node);

      // ADD TIMETRACKING TRIGGER
      if (module_exists('storm_dashboard')) {
        $node->content['stormticket_extension_timetracking_trigger'] = array(
        '#value' => theme('stormticket_extension_ticket_page_view_timetracking_trigger', $node),
        '#weight' => 1,
        );
      }

      break;
  }
}


/**
 * page view add ons
 *
 * @param unknown_type $node
 * @return unknown_type
 */
function stormticket_extension_page_view_adds($node) {
  $content = '';

  if (!empty($node)) {

    // --------------
    // DEPARTMENTS
    // --------------
    $department_terms = storm_contrib_common_get_node_taxonomy($node, storm_contrib_common_get_taxonomy_department());
    if (!empty($department_terms)) {
      $node->department_terms = $department_terms;
    }

    // --------
    // AUTHOR
    // --------
    if (!empty($node->uid) && is_numeric($node->uid)) {
      $author = user_load($node->uid);
      if (!empty($author->stormperson_nid)) {
        $author_node = node_load($author->stormperson_nid);
        if (!empty($author_node)) {
          $node->author_name = $author_node->title;
          $node->author_nid = $author_node->nid;
        }
        unset($author_node);
      }
      if (empty($node->author_name)) {
        $node->author_name = $author->name;
      }
      unset($author);
    }

    // ------
    // COSTS
    /// -----
    $main_currency = $node->currency;

    // ESTIMATED COSTS
    if (!empty($node->estimated_costs) && $node->estimated_costs_currency != $main_currency && module_exists('storm_contrib_common')) {
      $node->estimated_costs = storm_contrib_costs_convert_currency_exchange_rate($node->estimated_costs_currency, $main_currency, $node->estimated_costs);
      $node->estimated_costs_currency = $main_currency;
    }

    // ESTIMATED TIMETRACKING COSTS
    if (!empty($node->estimated_timetracking_costs) && $node->estimated_timetracking_costs_currency != $main_currency && module_exists('storm_contrib_common')) {
      $node->estimated_timetracking_costs = storm_contrib_costs_convert_currency_exchange_rate($node->estimated_timetracking_costs_currency, $main_currency, $node->estimated_timetracking_costs);
      $node->estimated_timetracking_costs_currency = $main_currency;
    }

    // TOTAL REAL COSTS
    if (!empty($node->total_real_costs) && $node->total_real_costs_currency != $main_currency && module_exists('storm_contrib_common')) {
      $node->total_real_costs = storm_contrib_costs_convert_currency_exchange_rate($node->total_real_costs_currency, $main_currency, $node->total_real_costs);
      $node->total_real_costs_currency = $main_currency;
    }

    // TOTAL REAL TIMETRACKING COSTS
    if (!empty($node->total_real_timetracking_costs) && $node->total_real_timetracking_costs_currency != $main_currency && module_exists('storm_contrib_common')) {
      $node->total_real_timetracking_costs = storm_contrib_costs_convert_currency_exchange_rate($node->total_real_timetracking_costs_currency, $main_currency, $node->total_real_timetracking_costs);
      $node->total_real_timetracking_costs_currency = $main_currency;
    }



    // ----------
    // DURATIONS
    // ----------

    // GET THE ASSIGNED TIMETRACKINGS
    if (module_exists('stormtimetracking_extension')) {
      $trackings = stormtimetracking_extension_get_trackings_of_type($node, FALSE);
    }

    // CONVERT THE REAL TIMETRACKING DURATIONS TO DURATIONUNIT OF TICKET
    if (!empty($trackings['total_duration_in_hours'])) {
      $node->total_real_duration = storm_contrib_common_convert_durations($trackings['total_duration_in_hours'], 'hours', $node->durationunit, 'work_day');
    }

    // ADD STILL OPEN DURATIONS AND REAL TIMETRACKING DURATIONS FOR DISPLAYING
    if (!empty($node->total_real_duration)) {
      $node->still_open_total_real_duration = $node->total_real_duration;
    }

  }

}



/**
 * check the dependency of duration and end date of the ticket
 *
 * @param unknown_type $node
 * @return unknown_type/stormticket_extension.module on line 318.
 */
function stormticket_extension_check_ticket_duration($node, $op, $type = 'work_day') {

  if (!empty($node)) {

    $result = storm_contrib_common_calculate_node_duration($node, $op, $type);

    // VALIDATE DURATION CALCULATION RESULTS
    storm_contrib_common_validate_duration($node, $result);

  }

}

/**
 * save if the duration of the ticket is a recurring event
 *
 * @param unknown_type $node
 */
function stormticket_extension_save_recurring_duration($node) {

  if (!empty($node)) {

    $object = new stdClass();
    $object->nid      = $node->nid;
    $object->vid      = $node->vid;
    $object->is_recurring_duration = $node->is_recurring_duration;

    $update = array();
    $count = db_result(db_query("SELECT COUNT(nid) FROM {stormticket_extension_recurring_duration} WHERE nid=%d AND vid=%d", $object->nid, $object->vid));
    if ($count > 0) {
      $update[] = 'nid';
      $update[] = 'vid';
    }
    drupal_write_record('stormticket_extension_recurring_duration', $object, $update);

  }

}

/**
 * save if the duration of the ticket is a recurring event
 *
 * @param unknown_type $node
 */
function stormticket_extension_load_recurring_duration($node) {

  if (!empty($node)) {

    $args = array();
    $sql  = 'SELECT is_recurring_duration ';
    $sql .= 'FROM {stormticket_extension_recurring_duration} ';
    $sql .= 'WHERE nid = %d ';
    $args[] = $node->nid;
    $sql .= 'AND vid = %d ';
    $args[] = $node->vid;
    $result = db_result(db_query(db_rewrite_sql($sql, 'stormticket_extension_recurring_duration', 'is_recurring_duration'), $args));
    $node->is_recurring_duration = $result;

  }

}


/**
 * get all assigned tickets of a project or task
 *  - depending on type if project or task
 *  - with or without tickets assigned to a task
 *
 * @param object $node
 * @param array $filter
 * - key: ticketstatus => array(inserted, in progress, ...)
 *
 * @return unknown_type
 */
function stormticket_extension_get_assigned_tickets($node, $without_assigned_tasks = FALSE, $filter = array(), $return_type = 'flat') {

  $tickets = array();

  if (!empty($node)) {

    $args = array();
    $column = '';

    switch ($node->type) {

      case 'stormproject':
        $filter['stormproject'] = $node->nid;
        break;

      case 'stormtask':
        $filter['stormtask'] = $node->nid;
        break;

    }

    $tickets = storm_contrib_common_get_stormtickets($filter, $return_type);

  }
  return $tickets;
}


/**
 * Implementation of hook_clone_node_alter() from the node_clone module
 *
 * @param unknown_type $node
 * @param unknown_type $original_node
 * @param unknown_type $op
 * @return unknown_type
 */
function stormticket_extension_clone_node_alter($node, $original_node, $op) {

  if (!empty($node) && !empty($original_node)) {

    switch ($op) {

      case 'prepopulate':

        storm_contrib_common_set_active_trail('storm/'. str_replace('storm', '', $original_node->type) .'s');

        $node->organization_nid = $original_node->organization_nid;
        $node->project_nid = $original_node->project_nid;
        $node->task_nid = $original_node->task_nid;
        $node->ticketcategory = $original_node->ticketcategory;
        $node->ticketstatus = $original_node->ticketstatus;
        $node->ticketpriority = $original_node->ticketpriority;
        $node->datebegin = $original_node->datebegin;
        $node->dateend = $original_node->dateend;
        $node->durationunit = $original_node->durationunit;
        $node->duration = $original_node->duration;
        $node->pricemode = $original_node->pricemode;
        $node->price = $original_node->price;
        $node->currency = $original_node->currency;
        $node->assigned_nid = $original_node->assigned_nid;

        break;

    }

  }
}


/**
 * pre render the task form before it is rendered as html
 * update the options of the select forms because they maybe were modified by javascript
 * but the form is fetched from the cache without the modifications
 *
 * @param unknown_type $form
 */
function stormticket_extension_pre_render_ticket_form($form) {

  // --------------------
  // CHECK ORGANIZATIONS
  // --------------------
  if (!empty($form['group1']['organization_nid']['#value']) && $form['group1']['organization_nid']['#value'] != $form['group1']['organization_nid']['#default_value']) {
    $form['group1']['organization_nid']['#default_value'] = $form['group1']['organization_nid']['#value'];
    // OPTIONS ARE THE SAME BECAUSE THEY ARE NOT CHANGED BY JS
  }

  // ----------------
  // CHECK PROJECTS
  // ----------------
  if (!empty($form['group1']['project_nid']['#value']) && ($form['group1']['project_nid']['#value'] != $form['group1']['project_nid']['#default_value'])) {

    $form['group1']['project_nid']['#default_value'] = $form['group1']['project_nid']['#value'];

  }

  // TODO MAYBE SOURCE OUT INTO SEPARATE FUNCTION
  // GET ONLY PROJECTS WITHOUT STATE COMPLETED
  $s = "SELECT n.nid, n.title FROM {stormproject} spr INNER JOIN {node} n ON spr.nid=n.nid WHERE spr.organization_nid=%d AND n.status=1 AND n.type='stormproject' AND NOT spr.projectstatus='completed' ORDER BY n.title";
  $s = stormproject_access_sql($s);
  $s = db_rewrite_sql($s);
  $r = db_query($s, $form['group1']['organization_nid']['#value']);
  $projects = array();
  while ($project = db_fetch_object($r)) {
    $projects[$project->nid] = $project->title;
  }
  $projects = array('' => '-') + $projects;
  $form['group1']['project_nid']['#options'] = $projects;

  return $form;
}


/**
 * validate stormticket node form
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @return unknown_type
 */
function stormticket_extension_ticket_node_form_validate($form, &$form_state) {

  // -------------------
  // CHECK ORGANIZATION
  // -------------------
  if (empty($form_state['values']['organization_nid']) || '' == $form_state['values']['organization_nid']) {
    form_set_error('organization_nid', t('You have to select an organization'));
  }

  // --------------
  // CHECK PROJECT
  // --------------
  if (empty($form_state['values']['project_nid']) || '' == $form_state['values']['project_nid']) {
    if (variable_get('stormproject_extension_ticket_project_mandantory', FALSE)) {
      form_set_error('project_nid', t('You have to select a project'));
    }
  }

}

function stormticket_extension_get_tickets($organization_nid = 0, $project_nid = 0, $task_nid = 0) {
  $tickets = array();

  $args = array();
  $where = array();
  $where[] = 'sti.organization_nid=%d';
  $args[] = $organization_nid;
  $where[] = 'sti.project_nid=%d';
  $args[] = $project_nid;
  $where[] = 'sti.task_nid=%d';
  $args[] = $task_nid;

  $s = "SELECT n.nid, n.title FROM {stormticket} sti INNER JOIN {node} n ON sti.nid=n.nid WHERE n.status=1 AND n.type='stormticket' ORDER BY n.title";
  global $user;
  $access_check = TRUE;
  if (!empty($project_nid) && module_exists('stormteam') && user_access('Storm ticket: view if project is assigned to team') && !empty($user->stormperson_nid)) {
    $project_team = storm_contrib_common_get_stormteam_members_from_project($project_nid);
    // we are member of the project team, so we have fully acces to all tickets from this project, so we do not need to call stormticket_access_sql
    if (array_key_exists($user->stormperson_nid, $project_team)) {
      $where[] = "'storm_access'='storm_access'";
      $s = storm_rewrite_sql($s, $where);
      $access_check = FALSE;
    }
  }
  if ($access_check) {
    $s = stormticket_access_sql($s, $where);
  }
  $s = db_rewrite_sql($s);
  $r = db_query($s, $args);
  while ($ticket = db_fetch_object($r)) {
    $tickets[$ticket->nid] = $ticket->title;
  }
  return $tickets;
}

/**
 * Implemenation of hook_menu()
 * @return unknown_type
 */
function stormticket_extension_menu() {

  $items = array();
  $items = storm_contrib_common_set_local_tasks($items, 'storm/tickets/');
  
  return $items;
}

/**
 * Implementation of hook_menu_alter
 * switch to our own js callback for added access control
 * @param  $items
 * @return void
 */
function stormticket_extension_menu_alter(&$items) {
  if (!empty($items) && module_exists('stormteam')) {
    $items['storm/task_tickets_js/%/%/%']['page callback'] = '_stormticket_extension_task_tickets_js';
  }
}

/**
 * return tickets as json for the given organization, project and task
 *
 * @param int $organization_nid
 * @param int $project_nid
 * @param int $task_nid
 * @return void
 */
function _stormticket_extension_task_tickets_js($organization_nid=0, $project_nid=0, $task_nid=0) {

  $tickets = stormticket_extension_get_tickets($organization_nid, $project_nid, $task_nid);

  print drupal_to_js($tickets);
  exit();
}

function stormticket_extension_content_extra_fields($type_name) {
  if ($type_name == 'stormticket') {
    return array(
      'stormticket_extension_add_ons_author' => array('label' => 'Storm Contrib Add ons Author', 'weight' => -13),
      'stormticket_extension_add_ons_timing' => array('label' => 'Storm Contrib Add ons Timing', 'weight' => -12.8),
      'stormticket_extension_add_ons_costs' => array('label' => 'Storm Contrib Add ons Costs', 'weight' => -12.6),
    );
  }
}

/**
 * Set the end date of the ticket, if it was not set by the author.
 *
 * @param object $node
 *   drupal node object of type stormticket
 * @param string $type
 *   work_day: use a work day duration for calculation, a work day duration can be set in the admin settings, default = 8 hours
 *   normal_day: use a normal day duration for calculation, a normal day has 24 hours
 */
function stormticket_extension_set_date_end_by_duration($node, $type) {

  if (!empty($node)) {

    $set_message = FALSE;

    $result = storm_contrib_common_calculate_node_duration($node, 'presave', $type);

    if (!empty($result['new_end_date_unix_time']) || $result['difference_in_seconds'] <= 0) {
      if (empty($result['new_end_date_unix_time']) && $result['difference_in_seconds'] <= 0) {
        if (!empty($result['time_unix_begin'])) {
          $result['new_end_date_unix_time'] = $result['time_unix_begin'];
        } else {
          $result['new_end_date_unix_time'] = time();
        }
      }
      if (storm_contrib_common_date_to_time($node->dateend) < $result['new_end_date_unix_time']) {
        if (is_array($node->dateend)) {
          if (!($node->dateend['day'] > 0)) {
            $node->dateend = storm_contrib_common_time_to_date($result['new_end_date_unix_time'], 'array');
            $set_message = TRUE;
          }
        }
        elseif(is_numeric($node->dateend)) {
          $node->dateend = $result['new_end_date_unix_time'];
          $set_message = TRUE;
        }
        else {
          $node->dateend = storm_contrib_common_time_to_date($result['new_end_date_unix_time'], storm_contrib_common_get_custom_system_date_format());
          $set_message = TRUE;
        }
      }
    }

    if ($set_message) {
      drupal_set_message(t('The end date has been automatically calculated and set to %value_date', array('%value_date' => storm_contrib_common_time_to_date($result['new_end_date_unix_time'], storm_contrib_common_get_custom_system_date_format()) )), 'status');
    }
  }

}