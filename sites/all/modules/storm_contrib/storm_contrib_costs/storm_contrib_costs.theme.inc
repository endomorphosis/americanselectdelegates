<?php

/**
 * @file storm_contrib_costs.theme.inc
 *
 * themeing functions for costs
 */


/**
 * display the estimated costs of a ticket, task or project
 *
 * @param unknown_type $estimated_costs
 * @return unknown_type
 */
function theme_storm_contrib_costs_costs_on_type($node) {

  if (!empty($node)) {

    // ESTIMATED COSTS
    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Estimated costs'), theme('storm_contrib_common_number_format', $node->estimated_costs) .' '. $node->estimated_costs_currency);
    $content .= '</div>';

    // ESTIMATED TIMETRACKING COSTS
    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Estimated timetracking costs'), theme('storm_contrib_common_number_format', $node->estimated_timetracking_costs) .' '. $node->estimated_timetracking_costs_currency);
    $content .= '</div>';

    // REAL COSTS TILL NOW
    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Real costs taken'), theme('storm_contrib_common_number_format', $node->total_real_costs) .' '. $node->total_real_costs_currency);
    $content .= '</div>';


    // ESTIMATED DURATION
    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Estimated duration'), format_plural($node->estimated_duration_in_hours, '@count hour', '@count hours'));
    $content .= '</div>';

    // REAL DURATION TILL NOW
    $content .= '<div class="stormfields">';
    $content .= theme('storm_view_item', t('Time taken'), format_plural($node->total_real_duration_in_hours, '@count hour', '@count hours'));
    $content .= '</div>';

  }

  return $content;
}


/**
 * theme costs
 *
 * @param unknown_type $node_organization
 * @param unknown_type $projects
 * @return unknown_type
 */
function theme_storm_contrib_costs_display_costs_stormorganization($node_organization, $projects) {

  $content = '';

  if (!empty($node_organization) && !empty($projects)) {

    drupal_add_css(drupal_get_path('module', 'storm_contrib_costs') .'/storm_contrib_costs.css');

    $class = 'balance-ok';

    $content .= '<div id="storm-contrib-costs-display-project-costs">';

    $header = array(t('Project'), t('Budget costs'), t('Estimated costs'), t('Real costs'));

    $rows = array();


    // -------------------
    // TOTAL RESULT ROW
    // -------------------
    $row = array();
    $row[] = array('data' => '<strong>'. t('Total') .'</strong>');

    // TOTAL BUDGET COSTS
    $row[] = array('data' => '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', $projects['total']['budget_costs']) .' '. $projects['total']['budget_costs_currency'] .'</span>' );

    // TOTAL ESTIMATED COSTS
    if ($projects['total']['estimated_costs'] > $projects['total']['budget_costs'] && !empty($projects['total']['budget_costs']) ) {
      $class = 'balance-error';
    }
    $row[] = array('data' => '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', $projects['total']['estimated_costs']) .' '. $projects['total']['estimated_costs_currency'] .'</span>' );
    $class = 'balance-ok';

    // TOTAL REAL COSTS
    if ($projects['total']['total_real_costs'] > $projects['total']['budget_costs'] && !empty($projects['total']['budget_costs'])) {
      $class = 'balance-error';
    }
    $row[] = array('data' => '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', $projects['total']['total_real_costs']) .' '. $projects['total']['total_real_costs_currency'] .'</span>' );
    $class = 'balance-ok';

    $rows[] = $row;


    // ----------
    // PROJECTS
    // ----------
    foreach ($projects as $nid_project => $project_array) {

      if (!empty($nid_project) && is_numeric($nid_project)) {

        $row = array();

        $class = 'balance-ok';
        $last_update = '';

        // PROJECT TITLE
        $row[] = array('data' => '<span class="'. $class .'">'. l($project_array['title'], 'node/'. $project_array['nid'] ) );

        // PROJECT BUDGET COSTS
        if (!empty($project_array['budget_costs'])) {
          $row[] = array('data' => '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', $project_array['budget_costs']) .' '. $project_array['budget_costs_currency'] .'</span>' );
        }
        else {
          $row[] = array('data' => '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', 0) .' '. $project_array['currency'] .'</span>' );
        }

        // PROJECT ESTIMATED COSTS
        if (!empty($project_array['estimated_costs'])) {
          if (!empty($project_array['budget_costs']) && $project_array['estimated_costs'] > $project_array['budget_costs']) {
            $class = 'balance-error';
          }
          if (!empty($project_array['estimated_costs_last_updated'])) {
            $last_update = '<span class="last-update">('. storm_contrib_common_time_to_date($project_array['estimated_costs_last_updated'], storm_contrib_common_get_custom_display_date_format() .' H:i:s' ) .')</span>';
          }
          $row[] = array('data' => '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', $project_array['estimated_costs']) .' '. $project_array['estimated_costs_currency'] .'</span> '. $last_update );
        }
        else {
          $row[] = array('data' => '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', 0) .' '. $project_array['currency'] .'</span>' );
        }

        $class = 'balance-ok';
        $last_update = '';

        // PROJECT TOTAL COSTS
        if (!empty($project_array['total_real_costs'])) {
          if (!empty($project_array['budget_costs']) && $project_array['total_real_costs'] > $project_array['budget_costs']) {
            $class = 'balance-error';
          }
          if (!empty($project_array['total_real_costs_last_updated'])) {
            $last_update = '<span class="last-update">('. storm_contrib_common_time_to_date($project_array['total_real_costs_last_updated'], storm_contrib_common_get_custom_display_date_format() .' H:i:s') .')</span>';
          }
          $row[] = array('data' => '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', $project_array['total_real_costs']) .' '. $project_array['total_real_costs_currency'] .'</span> '. $last_update );
        }
        else {
          $row[] = array('data' => '<span class="'. $class .'">'. theme('storm_contrib_common_number_format', 0) .' '. $project_array['currency'] .'</span>' );
        }

        $class = 'balance-ok';
        $last_update = '';

        $rows[] = $row;

      }

    }

    if (!empty($rows)) {
      $content .= theme('table', $header, $rows);
    }

    $content .= '</div>'; // END <div id="storm-contrib-costs-display-project-costs">

  }
  else {
    $content .= t('No projects available');
  }

  $content .= "<br />\n";

  return $content;
}


/**
 * display the current currency exchange rates to euro
 * @return unknown_type
 */
function theme_storm_contrib_costs_display_currency_exchange_rates($currency_names, $currencies_exchange_rates_to_euro) {

  $content = '';

  if (!empty($currencies_exchange_rates_to_euro)) {
    $content .= t('All currencies quoted against the euro (base currency)');
    $content .= "<br />\n";
    $content .= '<strong>'. t('1 Euro =') .'</strong>';
    $content .= "<br />\n";

    $header = array(t('Currency'), t('Description'), t('Exchange rate'), t('Last updated'));

    $rows = array();
    foreach ($currencies_exchange_rates_to_euro as $currency_code => $currency_array) {
      $row = array();
      $row[] = array('data' => $currency_code);
      $row[] = array('data' => $currency_names[$currency_code]);
      $row[] = array('data' => $currency_array['exchange_rate']);
      $row[] = array('data' =>  storm_contrib_common_time_to_date($currency_array['last_update'], storm_contrib_common_get_custom_display_date_format() .' - H:i:s'));
      $rows[] = $row;
    }

    if (!empty($rows)) {
      $content .= theme('table', $header, $rows);
    }

  }

  return $content;
}


/**
 * display costs of person on stormperson page view
 *
 * @param unknown_type $node
 * @return unknown_type
 */
function theme_storm_contrib_costs_stormperson_page_view_adds($node) {

  $content = '';

  if (!empty($node) && !empty($node)) {

    // COSTS PER UNIT
    if (!empty($node->person_costs)) {
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Costs per unit'), $node->person_costs_unit .' '. theme('storm_contrib_common_number_format', $node->person_costs) .' '. $node->person_costs_currency );
      $content .= '</div>';
    }

    // MONTHLY SALARY
    if (!empty($node->person_monthly_salary)) {
      $content .= '<div class="stormfields">';
      $content .= theme('storm_view_item', t('Monthly Salary'), theme('storm_contrib_common_number_format', $node->person_monthly_salary) .' '. $node->person_monthly_salary_currency );
      $content .= '</div>';
    }

  }

  return $content;
}