<?php

/**
 * @file stormtimetracking_extension.views_default.inc
 * Scans the 'default_views' subdirectory for default views
 */

/**
 * Implement hook_views_default_views().
 */
function stormtimetracking_extension_views_default_views() {
  // Search the "default_views" subdirectory for files ending in .view.php.
  $files = file_scan_directory(drupal_get_path('module', 'stormtimetracking_extension') .'/views/default_views', 'view\.php$');
  foreach ($files as $absolute => $file) {
    if ($file->name == "storm_c_timetracking_calendar.view.php" && !module_exists('calendar')) {
      continue;
    }
    require_once $absolute;
    if (isset($view)) {
      // $file->name has the ".php" stripped off, but still has the ".view".
      $view_name = substr($file->name, 0, strrpos($file->name, '.'));
      $views[$view_name] = $view;
    }
  }
  return $views;
}