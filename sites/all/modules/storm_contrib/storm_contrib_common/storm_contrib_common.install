<?php

/**
 * @file storm_contrib_common.install
 */


/**
 * Implementation of hook_install().
 *
 * Set custom date formats for storm.
 * @todo TODO check if hook_enable is called during installation - else call it in hook_install()
 *
 */
function storm_contrib_common_install() {

  // SET CUSTOM DATE FORMAT WITHOUT TIME
  if (module_exists('date')) {

    // @todo TODO review date formats - add possibility to use other country formats
    // @todo TODO not just german - maybe selection in installation profile
    variable_set('date_format_german_date_without_time', 'd. M. Y');
    variable_set('date_format_german_date_4_system', 'd. M. Y');
    variable_set('storm_contrib_common_custom_display_date_format', 'd. M. Y');
    variable_set('storm_contrib_common_custom_system_date_format', 'd. M. Y');

    switch ($GLOBALS['db_type']) {
      case 'mysql':
      case 'mysqli':

        db_query("INSERT INTO {date_format_types} (type, title, locked) VALUES ('%s', '%s', %d) ON DUPLICATE KEY UPDATE locked = %d", 'german_date_without_time', 'German date without time', 0, 0);
        db_query("INSERT INTO {date_format_types} (type, title, locked) VALUES ('%s', '%s', %d) ON DUPLICATE KEY UPDATE locked = %d", 'german_date_4_system', 'German date for system', 0, 0);
        db_query("INSERT INTO {date_formats} (format, type, locked) VALUES ('%s', '%s', %d) ON DUPLICATE KEY UPDATE locked = %d", 'd. M. Y', 'custom', 0, 0);

        break;
      case 'pgsql':

        db_query("UPDATE {date_format_types} SET locked = %d WHERE type = '%s' AND title = '%s'", 0, "german_date_without_time", "German date without time");
        if (!db_affected_rows()) {
          @db_query("INSERT INTO {date_format_types} (type, title, locked) VALUES ('%s', '%s', %d)", "german_date_without_time", "German date without time", 0);
        }

        db_query("UPDATE {date_format_types} SET locked = %d WHERE type = '%s' AND title = '%s'", 0, "german_date_4_system", "German date for system");
        if (!db_affected_rows()) {
          @db_query("INSERT INTO {date_format_types} (type, title, locked) VALUES ('%s', '%s', %d)", "german_date_4_system", "German date for system", 0);
        }

        db_query("UPDATE {date_formats} SET locked = %d WHERE format = '%s' AND type = '%s'", 0, "d. M. Y", "custom");
        if (!db_affected_rows()) {
          @db_query("INSERT INTO {date_formats} (format, type, locked) VALUES('%s', '%s',%d)", "d. M. Y", "custom", 0);
        }

        break;
    }

  }

  module_load_include('module', 'storm_contrib_common');
  storm_contrib_common_rebuild_menu_tree();

}


/**
 * Implementation of hook_enable().
 *
 * Create the taxonomy vocabularies for departments and skills
 * and add the first terms.
 *
 */
function storm_contrib_common_enable() {

  if (module_exists('taxonomy')) {

    // @todo TODO SOURCE OUT IN  OWN FUNCTION
    // @todo TODO MAYBE ADD ADMIN SETTINGS


    // ------------------------
    // VOCABULARY DEPARTMENT
    // ------------------------
    if ($vocabulary = taxonomy_vocabulary_load(variable_get('storm_contrib_common_vocabulary_department', 0))) {
      // Existing install. Add back forum node type, if the forums
      // vocabulary still exists. Keep all other node types intact there.
      $vocabulary = (array) $vocabulary;
      $vocabulary['nodes']['stormperson'] = 1;
      $vocabulary['nodes']['stormtask'] = 1;
      $vocabulary['nodes']['stormticket'] = 1;
      taxonomy_save_vocabulary($vocabulary);
    }
    else {
      // Create the forum vocabulary if it does not exist. Assign the vocabulary
      // a low weight so it will appear first in forum topic create and edit
      // forms.
      $vocabulary = array(
        'name' => t('Departments'),
        'multiple' => 1,
        'required' => 0,
        'hierarchy' => 1,
        'relations' => 0,
        'module' => 'storm_contrib_common',
        'weight' => -10,
        'nodes' => array('stormperson' => 1, 'stormtask' => 1, 'stormticket' => 1),  // TODO - CHECK IF THESE NODE TYPES ARE ENABLED
      );
      taxonomy_save_vocabulary($vocabulary);

      variable_set('storm_contrib_common_vocabulary_department', $vocabulary['vid']);

      // ADD TERMS
      $term_names = array('Office', 'IT', 'Graphics', 'Managment', 'Editorial');
      storm_contrib_common_insert_terms($vocabulary['vid'], $term_names);


    }


    // ------------------------
    // VOCABULARY SKILLS
    // ------------------------
    if ($vocabulary = taxonomy_vocabulary_load(variable_get('storm_contrib_common_vocabulary_skills', 0))) {
      // Existing install. Add back forum node type, if the forums
      // vocabulary still exists. Keep all other node types intact there.
      $vocabulary = (array) $vocabulary;
      $vocabulary['nodes']['stormperson'] = 1;
      taxonomy_save_vocabulary($vocabulary);
    }
    else {
      // Create the forum vocabulary if it does not exist. Assign the vocabulary
      // a low weight so it will appear first in forum topic create and edit
      // forms.
      $vocabulary = array(
        'name' => t('Skills'),
        'multiple' => 1,
        'required' => 0,
        'hierarchy' => 1,
        'relations' => 0,
        'module' => 'storm_contrib_common',
        'weight' => -10,
        'nodes' => array('stormperson' => 1), // TODO - CHECK IF NODE TYPE IS ENABLED
      );
      taxonomy_save_vocabulary($vocabulary);

      variable_set('storm_contrib_common_vocabulary_skills', $vocabulary['vid']);

      // ADD TERMS
      $term_names = array('PHP', 'JAVA', 'HTML', 'JavaScript', 'Flash', 'MS Office', 'Photoshop', 'Illustrator');
      storm_contrib_common_insert_terms($vocabulary['vid'], $term_names);
    }

  }

}

/**
 * Insert taxonomy vocabulary terms.
 *
 * @param int $vid
 *   the taxonomy vocabulary id
 * @param array $term_names
 *   an array of the term names that should be set in the vocabulary
 *
 */
function storm_contrib_common_insert_terms($vid, $term_names) {

  if (!empty($vid) && is_numeric($vid) && !empty($term_names) && is_array($term_names)) {
    foreach ($term_names as $term_name) {

      db_query("INSERT INTO {term_data} (vid, name) VALUES (%d, '%s')", $vid, $term_name);

      $tid = db_result(db_query("SELECT tid FROM {term_data} WHERE vid = %d AND name = '%s'", $vid, $term_name));
      if (!empty($tid)) {
        db_query("INSERT INTO {term_hierarchy} (tid, parent) VALUES (%d, 0)", $tid);
      }

    }

  }

}


/**
 * Implementation of hook_uninstall().
 *
 * Remove custom date formats of storm.
 *
 */
function storm_contrib_common_uninstall() {

  // RESET CUSTOM DATE FORMAT WITHOUT TIME
  if (module_exists('date')) {

    // @todo TODO better handling of custom time formats for other countries - see also hook_install()

    variable_del("storm_contrib_common_custom_display_date_format");
    variable_del('storm_contrib_common_custom_system_date_format');
    variable_del("date_format_german_date_without_time");
    variable_del("date_format_german_date_4_system");

    db_query("DELETE FROM {date_format_types} WHERE type = 'german_date_without_time'");
    db_query("DELETE FROM {date_format_types} WHERE type = 'german_date_4_system'");
    db_query("DELETE FROM {date_formats} WHERE format = 'd. M. Y' AND type = 'custom'");

  }

  // ------------------------
  // VOCABULARY DEPARTMENT
  // ------------------------
  if ($vocabulary = taxonomy_vocabulary_load(variable_get('storm_contrib_common_vocabulary_department', 0))) {
    taxonomy_del_vocabulary($vocabulary->vid);
  }

  // ------------------------
  // VOCABULARY SKILLS
  // ------------------------
  if ($vocabulary = taxonomy_vocabulary_load(variable_get('storm_contrib_common_vocabulary_skills', 0))) {
    taxonomy_del_vocabulary($vocabulary->vid);
  }

  db_query("DELETE FROM {menu_custom} WHERE menu_name = '%s'", 'storm-contrib-menu');
  db_query("DELETE FROM {menu_links} WHERE menu_name = '%s'", 'storm-contrib-menu');

}



/**
 * Implements hook_requirements().
 *
 * checks the correct version of the storm module
 * storm_contrib requires storm 6.x-2.x
 *
 * @param $phase
 * @return array $requirements
 */
function storm_contrib_common_requirements($phase) {

  $requirements = array();
  $requirements['storm_contrib'] = array();
  $requirements['storm_contrib']['title'] = t('Storm Contrib');

  $t = get_t();

  $path = drupal_get_path('module', 'storm');
  $info = drupal_parse_info_file($path .'/storm.info');

  if (empty($info['version'])) {
    $requirements['storm_contrib']['value'] = t('Unknown version of the storm module');
    $requirements['storm_contrib']['severity'] = REQUIREMENT_WARNING;
    $requirements['storm_contrib']['description'] = $t("Storm Contrib requires Storm-6.x-2.x module. You are probably using a version from the repository, so there is no version info available. Please make sure you have installed 6.x-2.x.");
    return $requirements;
  }

  $version = $info['version'];

  if (!empty($version)) {
    $str = substr($version,4,1);

    if (!empty($str)) {
      //checking the version
      $status = ($str >= 2);
    }

  }

  // Status 6.x-2.x for storm not found.
  if (!$status) {
    $requirements['storm_contrib']['value'] = t('Incorrect version of the storm module');
    $requirements['storm_contrib']['severity'] = REQUIREMENT_ERROR;
    $requirements['storm_contrib']['description'] = $t("Storm Contrib requires Storm-6.x-2.x module. Please download the storm version 6.x-2.x from <a href='@storm_url'>here</a>.", array('@storm_url' => 'http://drupal.org/project/storm'));
  }
  // storm status = 6.x-2.x - correct
  else {
    $requirements['storm_contrib']['value'] = t('Storm version is 6.x-2.x');
  }


  return $requirements;
}

function storm_contrib_common_update_6001(&$sandbox) {
  module_load_include('module', 'storm_contrib_common');
  storm_contrib_common_rebuild_menu_tree();

  return array(array('success' => TRUE, 'query' => 'Added storm menu.'));
}