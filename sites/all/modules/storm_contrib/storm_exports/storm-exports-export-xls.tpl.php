<?php
/**
 * @file storm-exports-export-xls.tpl.php
 *
 */

?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  </head>
  <body>
    <?php
    $table = theme('table', $header, $rows);
    $table = preg_replace('/<\/?(a|span) ?.*?>/', '', $table); // strip 'a' and 'span' tags
    print $table;
    ?>
  </body>
</html>
