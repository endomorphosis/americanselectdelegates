<?php
/**
 * @file stormticket-assignment-export-csv.tpl.php
 * Template to export a table as csv
 *
 */

// Print out header row, if option was selected.
if ($header) {
  print implode($seperator, $header) . "\r\n";
}

// Print out exported items.
if (!empty($rows)) {
  foreach ($rows as $count => $item_row):
    print implode($seperator, $item_row) . "\r\n";
  endforeach;
}
