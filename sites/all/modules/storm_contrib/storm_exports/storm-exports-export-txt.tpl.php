<?php
/**
 * @file storm-exports-export-txt.tpl.php
 *
 */


// Print out exported items.
if (!empty($rows)) {
  foreach ($rows as $count => $item_row) {
    $cnt = 0;
    foreach ($item_row as $field => $content) {
      if (!empty($header[$cnt])) {
        print $header[$cnt] .": "; 
        print strip_tags($content) . "\r\n"; // strip html so its plain txt.
      }
      $cnt++;
    }
    echo "\r\n";
  }
}


