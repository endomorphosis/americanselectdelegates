<?php

/**
 * @file stormperson_holidays.views.inc
 */


/**
 * Implementation of hook_views_data()
 * 
 * @return unknown_type
 */
function stormperson_holidays_views_data() {
  
  // --------------------------------------
  // TABLE STORMPERSON NUMBER OF HOLIDAYS
  // --------------------------------------
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['stormperson_number_of_holidays']['table']['group']  = 'Storm';
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['stormperson_number_of_holidays']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  
  // numeric text field number of holidays per year
  $data['stormperson_number_of_holidays']['number_of_holidays_per_year'] = array(
    'title' => t('Person Holidays - Number Of Holidays Per Year'),
    'help' => t('The number of holidays a person has per year.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // numeric text field number of holidays remaining from last year
  $data['stormperson_number_of_holidays']['number_holidays_remaining_last_year'] = array(
    'title' => t('Person Holidays - Number Of Holidays Remaining From Last Year.'),
    'help' => t('The number of holidays a person has remained from last year.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  // --------------------------------------
  // TABLE STORMPERSON HOLIDAYS
  // --------------------------------------
  
  // The 'group' index will be used as a prefix in the UI for any of this
  // table's fields, sort criteria, etc. so it's easy to tell where they came
  // from.
  $data['stormperson_holidays']['table']['group']  = 'Storm';
  
  // This table references the {node} table.
  // This creates an 'implicit' relationship to the node table, so that when 'Node'
  // is the base table, the fields are automatically available.
  $data['stormperson_holidays']['table']['join'] = array(
    // Index this array by the table name to which this table refers.
    // 'left_field' is the primary key in the referenced table.
    // 'field' is the foreign key in this table.
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );
  
  // timestamp field holiday
  $data['stormperson_holidays']['holiday'] = array(
    'title' => t('Person Holidays - Date Of Holiday'),
    'help' => t('The date when a person has holiday.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  
  // boolean field half day
  $data['stormperson_holidays']['half_day'] = array(
    'title' => t('Person Holidays - Half Day Holiday'),
    'help' => t('Whether the selected holiday is booked just for a half day.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Published'),
    'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // timestamp field request time
  $data['stormperson_holidays']['request_time'] = array(
    'title' => t('Person Holidays - Holiday Request Date'),
    'help' => t('The date when the person requested the holiday.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  // boolean field permitted
  $data['stormperson_holidays']['permitted'] = array(
    'title' => t('Person Holidays - Holiday Permitted'),
    'help' => t('Whether the selected holiday is permitted or not.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Published'),
    'type' => 'yes-no',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  
  // numeric text field permitted nid
  $data['stormperson_holidays']['permitted_nid'] = array(
    'title' => t('Person Holidays - Person Nid Who Permitted Holiday'),
    'help' => t('The nid of the person who permitted the holiday.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // timestamp field permitted time
  $data['stormperson_holidays']['permitted_time'] = array(
    'title' => t('Person Holidays - Date Of Holiday Permission'),
    'help' => t('The date when the holiday was permitted.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  
  
  return $data;
}

function stormperson_holidays_date_api_fields($field) {
  $values = array(
    'sql_type' => DATE_UNIX,
    'tz_handling' => 'site',
    'timezone_field' => '',
    'offset_field' => '',
    'related_fields' => array(),
    'granularity' => array('year', 'month', 'day'),
  );

  switch ($field) {
    case 'stormperson_holidays.holiday':
    case 'stormperson_holidays.permitted_time':
      return $values;
  }

}

/**
 * Implement hook_date_api_tables().
 */
function stormperson_holidays_date_api_tables() {
  return array('stormperson_holidays');
}