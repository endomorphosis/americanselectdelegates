<?php
/**
 * @file
 * Template to display a Views Slideshow Xtra link element.
 *
 * @ingroup themeable
 */
?>

<div<?php print drupal_attributes($attributes); ?>>
  <a class="<? print ($vsx['lightbox']) ? 'colorbox-load' : ''; ?>" href="<?php print $url; ?>"><?php print $text; ?></a>
</div>
