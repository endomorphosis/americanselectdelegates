<?php
/**
 * @file
 * Template to display a Views Slideshow Xtra text element.
 *
 * @ingroup themeable
 */
?>

<div<?php print drupal_attributes($attributes); ?>>
  <?php print $text; ?>
</div>
