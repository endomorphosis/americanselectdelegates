/**
 * @file
 * Views Slideshow Xtra Javascript.
 */
(function ($) {
  Drupal.behaviors.viewsSlideshowXtra = function(context) {
    // Find our views slideshow xtra elements
    $('[id^="views-slideshow-xtra-"]:not(.views-slideshow-xtra-processed)').addClass('views-slideshow-xtra-processed').each(function() {

      var uniqueID = $(this).attr('id').replace('views-slideshow-xtra-', '');

      // Get the slide box.
      var slideArea = $(this).parents('div.view-content:eq(0)').find('.views_slideshow_main');

      // Remove the views slideshow xtra html from the dom
      // Attach the views slideshow xtra html to below the main slide.
      $(this).appendTo(slideArea);

      // Get the offsets of the slide and the views slideshow xtra elements
      var slideAreaOffset = slideArea.offset();
      var xtraOffset = $(this).offset();
      var xtraHeight = $(this).height();

      // Move the views slideshow xtra element over the top of the slide.
      var marginMove = xtraOffset.top - slideAreaOffset.top;
      $(this).css({'top': xtraHeight + 'px'});

      // Move type=text slide elements into place.
      var slideData = Drupal.settings.viewsSlideshowXtra[uniqueID].slideInfo.text;
      var slideNum = 0;
      for (slide in slideData) {
        var items = slideData[slide];
        var itemNum = 0;
        for (item in items) {
          $('#views-slideshow-xtra-' + uniqueID + ' .views-slideshow-xtra-text-' + slideNum + '-' + itemNum).css({
            'width': slideArea.width()
          });
          itemNum++;
        }
        slideNum++;
      }

      // Move type=link slide elements into place.
      var slideData = Drupal.settings.viewsSlideshowXtra[uniqueID].slideInfo.link;
      var slideNum = 0;
      for (slide in slideData) {
        var items = slideData[slide];
        var itemNum = 0;
        for (item in items) {
        	$('#views-slideshow-xtra-' + uniqueID + ' .views-slideshow-xtra-link-' + slideNum + '-' + itemNum).css({
            'width': slideArea.width()
          });
          itemNum++;
        }
        slideNum++;
      }

      // Move type=image slide elements into place.
      var slideData = Drupal.settings.viewsSlideshowXtra[uniqueID].slideInfo.image;
      var slideNum = 0;
      for (slide in slideData) {
        var items = slideData[slide];
        var itemNum = 0;
        for (item in items) {
        	$('#views-slideshow-xtra-' + uniqueID + ' .views-slideshow-xtra-image-' + slideNum + '-' + itemNum).css({
            'width': slideArea.width()
          });
          itemNum++;
        }
        slideNum++;
      }

      var settings = Drupal.settings.viewsSlideshowXtra[uniqueID];
	    if (settings.pauseAfterMouseMove) {
	    	slideArea.mousemove(function(e) {
      	  if (pageX - e.pageX > 5 || pageY - e.pageY > 5) {
      	  	Drupal.viewsSlideshow.action({ "action": 'pause', "slideshowID": uniqueID });
      	    clearTimeout(timeout);
      	    timeout = setTimeout(function() {
      	    		Drupal.viewsSlideshow.action({ "action": 'play', "slideshowID": uniqueID });
      	    		}, 2000);
      	  }
      	  pageX = e.pageX;
      	  pageY = e.pageY;
	    	});
	    }

    });
  }

  Drupal.viewsSlideshowXtra = Drupal.viewsSlideshowXtra || {};

  var pageX = 0, pageY = 0, timeout;
  Drupal.viewsSlideshowXtra.transitionBegin = function (options) {
    var settings = Drupal.settings.viewsSlideshowXtra[options.slideshowID];

    // Hide elements either by fading or not
    if (settings.displayDelayFade) {
      $('#views-slideshow-xtra-' + options.slideshowID + ' .views-slideshow-xtra-row').fadeOut();
    }
    else {
      $('#views-slideshow-xtra-' + options.slideshowID + ' .views-slideshow-xtra-row').hide();
    }

    settings.currentTransitioningSlide = options.slideNum;

    // Pause from showing the text and links however long the user decides.
    setTimeout(function() {
      if (options.slideNum == settings.currentTransitioningSlide) {
        if (settings.displayDelayFade) {
          $('#views-slideshow-xtra-' + options.slideshowID + ' .views-slideshow-xtra-row-' + options.slideNum).fadeIn();
        }
        else {
          $('#views-slideshow-xtra-' + options.slideshowID + ' .views-slideshow-xtra-row-' + options.slideNum).show();
        }
      }
    },
    settings.displayDelay
    );
  };
})(jQuery);

/*
*/
