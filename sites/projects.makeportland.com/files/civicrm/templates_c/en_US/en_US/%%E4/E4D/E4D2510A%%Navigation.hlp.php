<?php /* Smarty version 2.6.26, created on 2012-09-20 23:56:36
         compiled from CRM/Admin/Page/Navigation.hlp */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('block', 'htxt', 'CRM/Admin/Page/Navigation.hlp', 26, false),array('block', 'ts', 'CRM/Admin/Page/Navigation.hlp', 28, false),)), $this); ?>
<?php $this->_tag_stack[] = array('htxt', array('id' => "id-navigation")); $_block_repeat=true;smarty_block_htxt($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
<p>
<?php $this->_tag_stack[] = array('ts', array()); $_block_repeat=true;smarty_block_ts($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>This screen shows the current CiviCRM navigation menu for your site - viewed as a "tree". The top-level menu items are listed
vertically. Click the triangle to the left of any top-level item ("branch") to expand it and see the menu items underneath it.
Remember that some menu items shown here will not be displayed in the live menu bar which appears at the top of each CiviCRM
screen. Users will only see menu items which they have permissions for, and menu items associated with disabled components
will also be hidden.<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_ts($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
</p>
<p></p>
<?php $this->_tag_stack[] = array('ts', array()); $_block_repeat=true;smarty_block_ts($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>You can customize the CiviCRM navigation menu bar to better meet the needs of your users. You can:
<ul>
<li>Add menu items at any level to support specific work-flows. For example, if your organization uses profile(s)
for data entry, you can add menu items linking to those profiles. New menu items can also link to external (non-CiviCRM)
web pages and web applications. Click "New Menu Item" to add an item at any level.</li>
<li>Rename menu items. Move your cursor over the menu item and right-click with your mouse. Then select "Rename".</li>
<li>Delete menu items that you don't use. Right-click and select "Delete".</li>
<li>Change the permissions for a menu item. Right-click and select "Edit".</li> 
<li>Re-order menu items (including moving them from one branch of the menu "tree" to another. Simply use your mouse to "drag
and drop" the menu item to the new location.</li>
</ul>

<p><strong>Changes you make to the menu are saved immediately. However, you will need reload this page to see your changes in
the menu bar above.</strong></p><?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_ts($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_htxt($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>